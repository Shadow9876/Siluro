pub mod components;
use std::collections::VecDeque;

use components::{player::{get_buf, random_value}, position::Pos3, tile::Lake};
use console_engine::{pixel, KeyCode, KeyModifiers};
use components::tile::Direction::*;
use crate::components::tile::Tile;

fn add_to_log(log: &mut [String;5],s: String) {
    for i in 0..log.len()-1 {
        log.swap(i, i+1);
    }
    log[log.len()-1]=s;
}
fn add_to_queue(log_queue: &mut VecDeque<String>, s: String) {
    log_queue.push_back(s)
}

fn main() {
    let mut engine = console_engine::ConsoleEngine::init_fill_require(120, 40, 10).unwrap();
    let mut l=Lake::new();

    let mut timer=0;
    let mut buf_timer=0;
    let mut in_game=false;
    let mut safehouse=false;
    let mut net=false;
    let mut predator=false;
    let mut enemy=None;
    let mut item=None;
    let mut item_eat=0;
    let mut character=false;
    let mut char_timer=0;
    let mut death=None;
    let mut fin=false;
    let mut input=None;
    let mut log=[();5].map(|_| String::new());
    let mut log_queue=VecDeque::new();
    let mut depth_fight=false;
    let mut depth_index=0;
    let mut depth_dest=0;
    let mut consume_timer=0;
    let mut mirror=false;
    let mut opening_timer=100;
    loop {
        engine.wait_frame(); // wait for next frame + capture inputs
        engine.check_resize(); // resize the terminal if its size has changed
        if engine.is_key_pressed_with_modifier(KeyCode::Char('c'), KeyModifiers::CONTROL,console_engine::KeyEventKind::Press) {
            break; // exits app
        }
        if !in_game && engine.is_key_pressed(KeyCode::Char('n')) {
            in_game=true;
        }
        else if in_game {
            if engine.is_key_pressed(KeyCode::Char('m')) {
                timer=0;
                buf_timer=0;
                in_game=false;
                safehouse=false;
                net=false;
                predator=false;
                enemy=None;
                item=None;
                item_eat=0;
                character=false;
                char_timer=0;
                death=None;
                l=Lake::new();
                fin=false;
                input=None;
                log=[();5].map(|_| String::new());
                log_queue=VecDeque::new();
                depth_fight=false;
                depth_index=0;
                depth_dest=0;
                consume_timer=0;
                mirror=false;
                opening_timer=100;
            }
            else if opening_timer<100 && opening_timer>0 {
                if engine.is_key_pressed(KeyCode::Char('s')) {
                    opening_timer=0;
                }
            }
            else if !l.player.is_alive() {
                death=l.player.death_cause;
            }
            else if enemy.is_some() {
                if engine.is_key_pressed(KeyCode::Char('1')) {input=Some(0);}
                if engine.is_key_pressed(KeyCode::Char('2')) {input=Some(1);}
                if engine.is_key_pressed(KeyCode::Char('3')) {input=Some(2);}
                if engine.is_key_pressed(KeyCode::Char('r')) {input=Some(3);}
                if engine.is_key_pressed(KeyCode::Char('7')) && l.player.get_inv_name(0).is_some() {
                    if !l.player.use_item_from_inventory(0) {
                        add_to_queue(&mut log_queue, String::from("CARP DROPPED WEAPON"));
                    }
                    l.player.inventory[0]=None;
                }
                if engine.is_key_pressed(KeyCode::Char('8')) && l.player.get_inv_name(1).is_some() {
                    if !l.player.use_item_from_inventory(1) {
                        add_to_queue(&mut log_queue, String::from("CARP DROPPED WEAPON"));
                    }
                    l.player.inventory[1]=None;
                }
                if engine.is_key_pressed(KeyCode::Char('9')) && l.player.get_inv_name(2).is_some() {
                    if !l.player.use_item_from_inventory(2) {
                        add_to_queue(&mut log_queue, String::from("CARP DROPPED WEAPON"));
                    }
                    l.player.inventory[2]=None;
                }
            }
            else if safehouse {
                if engine.is_key_pressed(KeyCode::Char('y')) {
                    l.player.rest();
                    safehouse=false;
                }
                if engine.is_key_pressed(KeyCode::Char('n')) {
                    safehouse=false;
                }
            }
            else if mirror {
                if engine.is_key_pressed(KeyCode::Char('r')) {
                    char_timer=0;
                    mirror=false;
                }
            }
            else if item.is_some() {
                if engine.is_key_pressed(KeyCode::Char('c')) || engine.is_key_pressed(KeyCode::Char('e')) {
                    if l.player.can_eat() {item_eat=1;}
                    else {add_to_queue(&mut log_queue, String::from("CARP COULD NOT EAT WITHOUT TEETH"))}
                }
                if engine.is_key_pressed(KeyCode::Char('s')) {item_eat=2;}
                if engine.is_key_pressed(KeyCode::Char('l')) {item_eat=3;}
            }
            else if character {
                if engine.is_key_pressed(KeyCode::Char('r')) {
                    character=false;
                    char_timer=0;
                }
            }
            else {
                let mut dir=None;
                if engine.is_key_pressed(KeyCode::Char('w')) {dir=Some(Forwards)}
                if engine.is_key_pressed(KeyCode::Char('s')) {dir=Some(Backwards)}
                if engine.is_key_pressed(KeyCode::Char('a')) {dir=Some(Left)}
                if engine.is_key_pressed(KeyCode::Char('d')) {dir=Some(Right)}
                if engine.is_key_pressed(KeyCode::Char('e')) {dir=Some(Up)}
                if engine.is_key_pressed(KeyCode::Char('q')) {dir=Some(Down)}
                if l.player.starving() && engine.is_key_pressed(KeyCode::Char('c')) {l.player.eat_self()}
                if engine.is_key_pressed(KeyCode::Char('7')) && l.player.get_inv_name(0).is_some() {
                    if !l.player.use_item_from_inventory(0) {
                        add_to_queue(&mut log_queue, String::from("CARP DROPPED WEAPON"));
                    }
                    l.player.inventory[0]=None;
                }
                if engine.is_key_pressed(KeyCode::Char('8')) && l.player.get_inv_name(1).is_some() {
                    if !l.player.use_item_from_inventory(1) {
                        add_to_queue(&mut log_queue, String::from("CARP DROPPED WEAPON"));
                    }
                    l.player.inventory[1]=None;
                }
                if engine.is_key_pressed(KeyCode::Char('9')) && l.player.get_inv_name(2).is_some() {
                    if !l.player.use_item_from_inventory(2) {
                        add_to_queue(&mut log_queue, String::from("CARP DROPPED WEAPON"));
                    }
                    l.player.inventory[2]=None;
                }
                if let Some(val)=dir {
                    match l.move_in_dir(val) {
                        components::tile::Action::Nothing => {},
                        components::tile::Action::SafeHouse => safehouse=true,
                        components::tile::Action::Net => net=true,
                        components::tile::Action::Predator => predator=true,
                        components::tile::Action::Fight(e) => enemy=Some(e),
                        components::tile::Action::Item(i) => item=Some(i),
                        components::tile::Action::Character => character=true,
                        components::tile::Action::DepthFight(e, d, i) => {
                            if let Some(val) = e {
                                depth_fight=true;
                                enemy=Some(val);
                                depth_index=d;
                                depth_dest=i;
                            }
                        },
                        components::tile::Action::Mirror => mirror=true,
                        components::tile::Action::Final => fin=true,
                        
                    }
                }
            }
        }
        engine.clear_screen();

        if !in_game {
            timer+=1;
            while timer>360 { timer-=240; }

            engine.print(10, if timer/3>40 {0} else {40-timer/3}, "
                                @@@@@                                                               
 @@@@@@@@@@@@@@@@               @@ @@@                                                              
  @@@@         @@              @     @@                                                             
   @@@@         @                    @@                                                             
    @@@@              @@@             @@         @@@@@    @@@           @@@@@@@          @@@@@@@    
     @@@@@           @@@@            @@@        @@@@@@@     @@@      @@@   @@@@@       @@      @@@  
      @@@@@           @@@           @@@@        @   @@@      @@@    @@@      @@@@     @@@       @@@ 
       @@             @@@          @@@ @@           @@@       @@@   @@@       @@@@   @@@        @@@@
      @@              @@@         @@@@ @@           @@@       @@@   @@@        @@@   @@@         @@@
     @@               @@@        @@@@   @@          @@@       @@@   @@@        @@@   @@@@        @@@
   @@            @@   @@@       @@@@    @@    @@    @@@      @@@    @@@       @@@    @@@@       @@@ 
  @@          @@@@    @@@   @   @@@      @@   @     @@@      @@@    @@@@      @@      @@@@      @@  
 @@@@@@@@@@@@@@@@@    @@@@@@   @@@       @@@@@@      @@@@ @@@@      @@@@@@@@@@@         @@@@  @@@   
                                                                    @@@                             
                                                                    @@@                             
                                                                    @@@                             
                                                                    @@@                             
");

            if timer>=120 && timer%24>=12 {
                engine.rect(50, 15, 72, 19, pixel::pxl('#'));
                engine.print(54, 17, "<N> - NEW GAME");
            }
        }
        else if opening_timer>0 {
            match opening_timer {
                70..=99 => engine.print(49, 15, "JUST A FISH IN THE SEA"),
                40..=69 => engine.print(50, 15, "WHERE ARE YOU GOING?"),
                10..=39 => engine.print(49, 15, "THE DEPTHS ARE WAITING"),
                _ => {}
            }
            if opening_timer>0 {
                opening_timer-=1;
            }
            
            engine.print(55, 25, "<S> - SKIP");
        }
        else if fin {
            engine.print(0, 0, "CARP FOUND THE CORPSE OF THE KING");
            engine.print(0, 2, "CARP WAS CRUSHED BY PRESSURE");
            engine.print(0, 4, "GAME OVER");
            engine.print(0, 6, "<M> - MAIN MENU");
        }
        else if let Some(val) = death {
            match val {
                components::player::DeathCause::Health => engine.print(0, 0, "CARP COLLAPSED BECAUSE OF ITS INJURIES"),
                components::player::DeathCause::Hunger => engine.print(0, 0, "CARP STARVED TO DEATH"),
                components::player::DeathCause::Mind => engine.print(0, 0, "CARP WENT INSANE"),
                components::player::DeathCause::Brain => engine.print(0, 0, "CARP SUFFERRED IRREPARABLE BRAIN DAMAGE"),
                components::player::DeathCause::Gills => engine.print(0, 0, "CARP SLOWLY DROWNED"),
                components::player::DeathCause::Skeleton => engine.print(0, 0, "CARP'S SKELETON WAS CRUSHED BY PRESSURE"),
                components::player::DeathCause::Flesh => engine.print(0, 0, "CARP'S FLESH WAS TORN ASUNDER"),
                components::player::DeathCause::Blood => engine.print(0, 0, "CARP IS EXHAUSTED, CARP IS SLEEPING"),
                components::player::DeathCause::Skin => engine.print(0, 0, "CARP'S SKIN WAS FLAYED OFF"),
                components::player::DeathCause::Hearth => engine.print(0, 0, "CARP SUFFERED A HEARTH ATTACK"),
                components::player::DeathCause::Liver => engine.print(0, 0, "CARP'S LIVER GAVE UP RESULTING IN A SLOW AND PAINFUL DEATH"),
                components::player::DeathCause::Kidneys => engine.print(0, 0, "CARP'S KIDNEYS GAVE UP RESULTING IN A PROLONGED AND PAINFUL DEATH"),
                components::player::DeathCause::Pancreas => engine.print(0, 0, "CARP WAS EATEN FROM THE INSIDE"),
                components::player::DeathCause::Intestine => engine.print(0, 0, "CARP'S INTESTINES WERE DESTROYED"),
                components::player::DeathCause::Stomach => engine.print(0, 0, "CARP'S STOMACH WAS DESTROYED"),
            }
            engine.print(0, 2, "GAME OVER");
            engine.print(0, 4, "<M> - MAIN MENU");
        }
        else if net {
            engine.print(0, 0, "CARP WAS FISHED OUT OF THE LAKE");
            engine.print(0, 2, "GAME OVER");
            engine.print(0, 4, "<M> - MAIN MENU");
        }
        else if predator {
            engine.print(0, 0, "CARP WAS EATEN BY A PREDATOR");
            engine.print(0, 2, "GAME OVER");
            engine.print(0, 4, "<M> - MAIN MENU");
        }
        else if safehouse {
            engine.print(0, 0, "THIS IS A SAFE PLACE TO REST");
            engine.print(0, 2, "<Y> - REST");
            engine.print(0, 4, "<N> - DO NOT REST");
        }
        else if mirror {
            if char_timer<24 {
                engine.print(0, 0, "                                         #*+++++******##################%%%%%%%%%%%%%%%%%%%%%########+*****##***#######
                                      *++*++++*******#########%%%#%%#######%%%%#%%%%%%%%%%###%##*####*+++*##**+**######
                                   *++*+++++++***#######%%##########################%%%%%%%%#####**########*****+######
                                *++++++++*****############%%%####%%######%%#####%#%%%%%%##########**########*******####
                             +++*++********################%%%####%%#####%%####%###################**####*+**#******###
                         **++**++++***##############%%%%%%%%%%######################################**####**+***#***###
                      @%#**+++**####%%%%%%%%%##%%%####################################################**###*++*#**+*+*#
                  @@@@%%****###%####%%%#############*#%%#*****########################################*+**#*********++*
                 @**%%*##%##########*#%#%%%############%%%#*****############################**##*#####******+++**+***+*
                @%%%%%%%%%%%%%%####%#**###%#****######%######****##############*######***********##*###*****+++++*++***
               @@@%%@@@@@%%%########%%#%#%*#*****###***#######****##*#*###############*************####**#***++++**+==+
              @@%#@%%%%@%@@%%%#%%%###**#####*****##**++++++********#*++***#######*#***##*************###*###**+++*++===
             #+****#####**+*#%%%%%%%%##%#####****#%%##*-#%%+*#*************#####*###***#******************#%##****+++==
           #%%*++*++++***#%##%%%%#%###########****##%#+#@@@*#%*******#*##*######*#*#*#***********#****************+++==
          ##+*##+*####%###**##################***#*####=*#+%##******#########*##***####***************************+++==
        ######%#######*+%#*********###############**##########*****##**#####**##*************#********************+++==
       %%%****#########**%**#********#########################****#########****#******#*********#*****************+++==
       #***+++**######**+*%%%#************#*##**********####**#*##*##**#####*###*****************#***************++++==
      @++===+**#%#****#**+=*%####*********#*************#***###*#****#####***###*************##*###**************+=====
      #+=----===++*##%#*##*+=**####+********#*************#*#*#******##########%**************#####**####*#*+*****+====
      **++=---=====+*#**#****+++++**++***************************##################***********###########**#####***=+++
     ***+++===========**#*%***+==****+++++*********************#######%##############***##*#############****#**###*+===
     ##++**+========---==*%%***+==++***+++******************########%%%#############********###########%#*+*#####**+==+
    %%####******++++====-=##*+**+==+++++*********###**#############%%%######*######**********##########*#+++%%##**+====
 %##%#*##*******++++++++==+*+****===+++++********###*##*###########%%%#####*#+*####**###*****#########***++*####**+====
##%%%@@%@@@@@@@@@@*+++=++++*+*++++=+++++********############################++####**##******##########**=+*#####*+++**+
 %%%%%%@@@@@@@@@@@@@@@*++++*++*+*+++++***###****###########################+**#####*#*#######**#####*+*+++###%#**=*++++
 *%%%%@@%%%@%%%%%###%%@@#*+**+****=++**###############################%###+*##*######**###**#######*+**++*##%%###**++++
  *#*@%%###**********%%%%#**#*#**++++*###########%##################%%%%+**###*#####**##***#######*+**=+**#*%%***++++*+
  *##*%%##*****++***##%%@%%#%*#**++**#####%%%%%%%%###########*#########+**####*#######**########***##++*##*#*#*******++
  *%%#%@@@%%%#######%%%%@@@@%%%%##%%%%%%%%%%%%%%%%%%%%%%                                                               ")
            }
            else {
                engine.print(0, 0, "YOU ARE CARP");
                engine.print(0, 2, &format!("HEALTH: {}",l.player.health));
                engine.print(0, 3, &format!("HUNGER: {}",l.player.hunger));
                engine.print(0, 4, &format!("MIND: {}",l.player.mind));
                engine.print(0, 5, &format!("TEETH: {}",l.player.body_parts.face.teeth));
                engine.print(0, 6, &format!("BRAIN: {}",l.player.body_parts.face.brain));
                engine.print(0, 7, &format!("GILLS: {}",l.player.body_parts.face.gills));
                engine.print(0, 8, &format!("LEFT EYE: {}",l.player.body_parts.face.eyes.0));
                engine.print(0, 9, &format!("RIGHT EYE: {}",l.player.body_parts.face.eyes.1));
                engine.print(0, 10, &format!("BLOOD: {}",l.player.body_parts.torso.blood));
                engine.print(0, 11, &format!("LEFT FIN: {}",l.player.body_parts.torso.fins[0]));
                engine.print(0, 12, &format!("RIGHT FIN: {}",l.player.body_parts.torso.fins[1]));
                engine.print(0, 13, &format!("BACK LEFT FIN: {}",l.player.body_parts.torso.fins[2]));
                engine.print(0, 14, &format!("BACK RIGHT FIN: {}",l.player.body_parts.torso.fins[3]));
                engine.print(0, 15, &format!("UPPER FIN: {}",l.player.body_parts.torso.fins[4]));
                engine.print(0, 16, &format!("LOWER FIN: {}",l.player.body_parts.torso.fins[5]));
                engine.print(0, 17, &format!("TAIL: {}",l.player.body_parts.torso.fins[6]));
                engine.print(0, 18, &format!("FLESH: {}",l.player.body_parts.torso.flesh));
                engine.print(0, 19, &format!("SKELETON: {}",l.player.body_parts.torso.skeleton));
                engine.print(0, 20, &format!("SKIN: {}",l.player.body_parts.torso.skin));
                engine.print(0, 21, &format!("HEARTH: {}",l.player.body_parts.internal.hearth));
                engine.print(0, 22, &format!("INTESTINE: {}",l.player.body_parts.internal.intestine));
                engine.print(0, 23, &format!("KIDNEYS: {}",l.player.body_parts.internal.kidneys));
                engine.print(0, 24, &format!("LIVER: {}",l.player.body_parts.internal.liver));
                engine.print(0, 25, &format!("PANCREAS: {}",l.player.body_parts.internal.pancreas));
                engine.print(0, 26, &format!("SPLEEN: {}",l.player.body_parts.internal.spleen));
                engine.print(0, 27, &format!("STOMACH: {}",l.player.body_parts.internal.stomach));
                engine.print(0, 28, &format!("SWIM BLADDER: {}",l.player.body_parts.internal.swim_bladder));
                engine.print(53, 30, "<R> - RETURN");
            }
            char_timer+=1;
            while char_timer>48 {
                char_timer-=24;
            }
        }
        else if let Some(e) = &mut enemy {
            engine.print(0, 0, &e.print_opponent());
            let v=e.return_limbs();
            let mut ind=30;
            if v.contains(&0) {engine.print(53, ind, "<1> - ATTACK TORSO"); ind+=1;}
            else if input==Some(0) {input=None;}
            if v.contains(&1) {engine.print(53, ind, "<2> - ATTACK HEAD"); ind+=1;}
            else if input==Some(1) {input=None;}
            if v.contains(&2) {engine.print(53, ind, "<3> - ATTACK TAIL"); ind+=1;}
            else if input==Some(2) {input=None;}
            let i1=l.player.get_inv_name(0);
            let i2=l.player.get_inv_name(1);
            let i3=l.player.get_inv_name(2);
            if i1.is_some() {engine.print(53, ind, &format!("<7> - {}",i1.unwrap())); ind+=1;}
            if i2.is_some() {engine.print(53, ind, &format!("<8> - {}",i2.unwrap())); ind+=1;}
            if i3.is_some() {engine.print(53, ind, &format!("<9> - {}",i3.unwrap())); ind+=1;}
            engine.print(53, ind, "<R> - RETREAT");
            if let Some(i)=input {
                match i {
                    3 => {
                        if l.player.are_fins_functional() && l.player.is_brain_functional() && random_value(0, 10)<5 || random_value(0, 10)<1 {
                            if !depth_fight {add_to_queue(&mut log_queue, String::from("CARP RETREATED"));}
                            else {add_to_queue(&mut log_queue, String::from("CARP RETREATED DEEPER"));}
                            if !depth_fight {
                                l.set_pos(l.player.position,Tile::Enemy(enemy.unwrap()));
                                l.player.position=l.prev_player_pos;
                            }
                            else {
                                l.player.position=Pos3::new(l.player.position.x, depth_index, l.player.position.z);
                                l.depth_guards[depth_index]=enemy;
                                depth_fight=false;
                                depth_index=0;
                            }
                            enemy=None;
                        }
                        else {
                            add_to_queue(&mut log_queue, String::from("CARP COULD NOT RETREAT"));
                            for damage in e.deal_damage() {
                                l.player.damage(damage, random_value(0, 100)<if !depth_fight {2} else {15})
                            }
                        }
                    }
                    _ => {
                        if e.receive_damage(l.player.cause_damage(), i) || e.retreat() {
                            enemy=None;
                            if !depth_fight {l.set_pos(l.player.position,if l.player.depth()!=10 {Tile::Water} else {Tile::Floor});}
                            else {
                                l.player.position=Pos3::new(l.player.position.x, depth_dest, l.player.position.z);
                                l.depth_guards[depth_index]=None;
                                depth_fight=false;
                                depth_index=0;
                                depth_dest=0;
                            }
                        }
                        else {
                            for damage in e.deal_damage() {
                                l.player.damage(damage, random_value(0, 100)<if !depth_fight {2} else {30})
                            }
                        }
                    }
                }
                input=None;
            }
        }
        else if let Some(val) = &item {
            match val {
                components::item::Item::Food(f) => {
                    let food=
                    match f {
                        components::item::Food::Insect(_) => "insects",
                        components::item::Food::Worm(_) => "worms",
                        components::item::Food::Crayfish(_) => "delicious crayfish",
                        components::item::Food::Snail(_) => "snails",
                        components::item::Food::Berry(_) => "berries",
                        components::item::Food::Seed(_) => "seeds",
                        components::item::Food::Bread(_) => "yeasty bread",
                    };
                    engine.print(0, 0, &format!("Carp finds some {food}").to_uppercase());
                    engine.print(0, 2, "<E> - EAT IT");
                    engine.print(0, 4, "<S> - STASH IT");
                    engine.print(0, 6, "<L> - LEAVE IT");
                },
                components::item::Item::Weapon(w) => {
                    match w {
                        components::item::Weapon::Stick { damage, .. } => {
                            engine.print(0, 0, &format!("Carp finds a stick ({damage}) while Carp's teeth are ({}) sharp",l.player.teeth.get_damage()).to_uppercase());
                            engine.print(0, 2, "<S> - STASH IT");
                            engine.print(0, 4, "<L> - LEAVE IT");
                        },
                        components::item::Weapon::Knife { damage, .. } => {
                            engine.print(0, 0, &format!("Carp finds a knife ({damage}) while Carp's teeth are ({}) sharp",l.player.teeth.get_damage()).to_uppercase());
                            engine.print(0, 2, "<S> - STASH IT");
                            engine.print(0, 4, "<L> - LEAVE IT");
                        },
                        _ => {}
                    }
                },
                components::item::Item::Potion(p) => {
                    let potion=
                    match p {
                        components::item::Potion::LesserHealing(_) => "red liquid",
                        components::item::Potion::Healing(_) => "velvet liquid",
                        components::item::Potion::GreaterHealing(_) => "pink liquid",
                        components::item::Potion::LesserSanity(_) => "blue liquid",
                        components::item::Potion::Sanity(_) => "white liquid",
                        components::item::Potion::Masochism(_) => "black liquid",
                    };
                    engine.print(0, 0, &format!("Carp finds some {potion}").to_uppercase());
                    engine.print(0, 2, "<C> - CONSUME IT");
                    engine.print(0, 4, "<S> - STASH IT");
                    engine.print(0, 6, "<L> - LEAVE IT");
                },
            }
            match item_eat {
                0 => {},
                1 => {
                    item_eat=0;
                    let v=l.player.use_item(item.clone().unwrap());
                    if v {item=None; l.set_pos(l.player.position, if l.player.depth()!=10 {Tile::Water} else {Tile::Floor})}
                },
                2 => {
                    item_eat=0;
                    let v=l.player.stash_item(item.clone().unwrap());
                    if v {item=None; l.set_pos(l.player.position, if l.player.depth()!=10 {Tile::Water} else {Tile::Floor})}
                    else {add_to_queue(&mut log_queue, String::from("CARP'S INVENTORY WAS FULL"))}
                },
                3 => {
                    item_eat=0;
                    item=None;
                },
                _ => unreachable!()
            }
        }
        else if character {
            if char_timer<24 {
                engine.print(0, 0, "                                         #*+++++******##################%%%%%%%%%%%%%%%%%%%%%########+*****##***#######
                                      *++*++++*******#########%%%#%%#######%%%%#%%%%%%%%%%###%##*####*+++*##**+**######
                                   *++*+++++++***#######%%##########################%%%%%%%%#####**########*****+######
                                *++++++++*****############%%%####%%######%%#####%#%%%%%%##########**########*******####
                             +++*++********################%%%####%%#####%%####%###################**####*+**#******###
                         **++**++++***##############%%%%%%%%%%######################################**####**+***#***###
                      @%#**+++**####%%%%%%%%%##%%%####################################################**###*++*#**+*+*#
                  @@@@%%****###%####%%%#############*#%%#*****########################################*+**#*********++*
                 @**%%*##%##########*#%#%%%############%%%#*****############################**##*#####******+++**+***+*
                @%%%%%%%%%%%%%%####%#**###%#****######%######****##############*######***********##*###*****+++++*++***
               @@@%%@@@@@%%%########%%#%#%*#*****###***#######****##*#*###############*************####**#***++++**+==+
              @@%#@%%%%@%@@%%%#%%%###**#####*****##**++++++********#*++***#######*#***##*************###*###**+++*++===
             #+****#####**+*#%%%%%%%%##%#####****#%%##*-#%%+*#*************#####*###***#******************#%##****+++==
           #%%*++*++++***#%##%%%%#%###########****##%#+#@@@*#%*******#*##*######*#*#*#***********#****************+++==
          ##+*##+*####%###**##################***#*####=*#+%##******#########*##***####***************************+++==
        ######%#######*+%#*********###############**##########*****##**#####**##*************#********************+++==
       %%%****#########**%**#********#########################****#########****#******#*********#*****************+++==
       #***+++**######**+*%%%#************#*##**********####**#*##*##**#####*###*****************#***************++++==
      @++===+**#%#****#**+=*%####*********#*************#***###*#****#####***###*************##*###**************+=====
      #+=----===++*##%#*##*+=**####+********#*************#*#*#******##########%**************#####**####*#*+*****+====
      **++=---=====+*#**#****+++++**++***************************##################***********###########**#####***=+++
     ***+++===========**#*%***+==****+++++*********************#######%##############***##*#############****#**###*+===
     ##++**+========---==*%%***+==++***+++******************########%%%#############********###########%#*+*#####**+==+
    %%####******++++====-=##*+**+==+++++*********###**#############%%%######*######**********##########*#+++%%##**+====
 %##%#*##*******++++++++==+*+****===+++++********###*##*###########%%%#####*#+*####**###*****#########***++*####**+====
##%%%@@%@@@@@@@@@@*+++=++++*+*++++=+++++********############################++####**##******##########**=+*#####*+++**+
 %%%%%%@@@@@@@@@@@@@@@*++++*++*+*+++++***###****###########################+**#####*#*#######**#####*+*+++###%#**=*++++
 *%%%%@@%%%@%%%%%###%%@@#*+**+****=++**###############################%###+*##*######**###**#######*+**++*##%%###**++++
  *#*@%%###**********%%%%#**#*#**++++*###########%##################%%%%+**###*#####**##***#######*+**=+**#*%%***++++*+
  *##*%%##*****++***##%%@%%#%*#**++**#####%%%%%%%%###########*#########+**####*#######**########***##++*##*#*#*******++
  *%%#%@@@%%%#######%%%%@@@@%%%%##%%%%%%%%%%%%%%%%%%%%%%                                                               ")
            }
            else {
                engine.print(0, 0, "#**+*=#+%#%%+#*%%####@@@@           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                          
+++*=*#=#%%*@*##%%%%*+%%@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                  
**+**=*+###%###@##***%@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@            
--==++*+#+%+*#*+-=#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@%@@@@@%@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@+       
---++#+#**+=--*#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@%@@@@@@@@%@@@@@@@@@@@@@@@@@@+%@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@=    
=*%+#*+---=+%@@@@@@@@@@@@@@@@%@@@@@@@@@#@@@@@@@@@@@@@@@@@@@@@*@@@@@@@@@=@@@#*@@@@@@@@@@@@@@@@@@@@@@%#@@@@@@+@@@@@@@@@  
+#%+*--=+##%@@@%@@@@@%@@@@@@%@#@@@%@@@@%@@@@@@@@@@@%@@@@@@@#@@@@@@@#%@@@@*+@%@@@@@@@@@@#@@@@@@@@@@+*+%+###+++%@@@@@@@@=
%=--==+**#%%%%%@%@%@%@%@@#@*@@@@@#@%@@+@@%@%@@%@#@@@@%@@=@@@@+@@@@@@%@%@@@@@@@@@@@@@@@@@@@@@@*#-----+%*%+%%@@@@%#%@@@@ 
=-=++=*##%%*%*%#@%%#@@#%%%@%%%%#%%%%@%%###%##%%%%#%%@@#%@@@@@@@@%%%@@%%@@@@@@@@@@@@@@@@@@@@@@%+#:=@*@+#%%#%@#@##%**%#- 
+===+***######+#+##%##%%#**##%@%%##%###%#%%%%%%%%%@%@#@%@%@%@#%%%%%@%@@@@@@@@%@@@@@@@@@@@%@@##@@@@@@=*%@%@+%%%@@%*##@@ 
+++=+=+*+***#***+*+#**#*+********###+##***#######*@%#%%#%##@%%%%%%%%#%@@@@@@@@@@@@@@@@@%@@@###**#+#@@@*@%@@%@##%@@%%#@@
+====++++=+++===+++*++*++*+*++*********#%*@#+%#*#*#%%@%#%#@%#%#@%#*%#@@@@%%@@@@@@@@@@@@@@%%##@%%@@@%@@@%%@*#@@@@##+==+ 
==-===--==========+++++++=#*++**#+++******+**+*###*%*%%%##%%%##%%*##%%@@%%%%@%@@@@@@@@@%@@%#%@%*@@%@@@%@@@@@@%#@%%*#+  
---------=-==========+**++=++++++#+++**+*#**###*####**%*#*#####@##**%%#%%%%%%@@@%@%%@%%%%@%%%*#%@@@@@@@@@@@%@@@@%@=    
---------====--====+++++*+=++*+*#+++++****#****#*###***#*####%####*#@%%##%%%%%%%@@@%@@%@%@%@#%#@@@@%@@%@@@@@@@%%       
---------=-========++++**+*=++++++++++****++*#**##%#%#**##%#*#%%%%**%%%%###%#@%@@%@@@@@@@@@@@@@@@@@@@@@@@@@@@          
-----------=-=---====++*+*+++*++++****+*#*******##**#%##**%@*%%*#*#*@@@%%#####@%%@@@@@@@@@@@@@@@@@@@@@@@@@-            
-----:-------=-=-====+++++++++++****#+#*+***+*#**##*#*+%#*+#**####*##%@@@%%#%#%@@@@@@@@@@@@@@@@@@@@@@@#-               
--------------======++==+**+***+*+**#*##*+#**###%%#%%%*%%*#**%#*++***#**#%%%%%@@@@@@@%@@%@@@@@@@@@%-                   
-::--------=---=====++=++*##*++***#**%*#*####%#%#%%@@%%#+++++**+++**+=--+@@@@@@@@@@@@@@@@@@@@@%*                       
-------==========+++++****##*##*#%%###%%%%%%%%@@@@@@@%##*++===++++***=-+*+#+%@@@%%@@@@@@%%@%                           
-----==-====+=+++=*+**+*****#**%%%#%%%%@@@%%@@@@@@%@%%%%%####%@@%@%%####@%@%%@@@@@%%*##+                               
----======++=+**=+++*****####%%%%%%%%%%@@@@@@@@@@@@@@@@%%%@@%%%%@%*#%##@@@@@@@@@@@*-                                   
----==+=+++*+*#**++*+**######%#%%%%%@@@@@@@@@@@@@@@@@@@@@@@@@@@@%@@@@@@@@@@@@@*=                                       
=======++++*+**#********###%%%%%%%@%@@@@@@@@@@@%%%@@@@@@@@@@@@@@@@@@@@@@@#=-                                           
-========++++++*++*+***#*#*###%#%@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@%+-                                                 
=--=-=========+=++++*****+#######%%%%%%%@@%@@%##@@@@@@@@@@@@@@*-                                                       
----=-=-------:::::::--=++#*#*##***#%%#%%%###%@@@@@@@@@@@@@@                                                           
:--------:::::::::::::::-=+**+++=+++++==+=+##%@@@@@@@@@@@                                                              
:::::::::::::::::::::----@@@@%#           %@@@@@@@@@@@@                                                                
:-::::::::::::::------::                %@@@@@@@@@@@                                                                   ");
                engine.print(0, 32, "I'M SUBERE OF THE DEPTHS");
                engine.print(0, 33, 
                    match l.player.depth() {
                        4 => "THE SURFACE BORES ME... COME DEEPER, THERE ARE STILL MUCH TO LEARN",
                        5 => "THE OLD KING WAS CERTAINLY... PECULIAR... NO ONE FOUND HIM YET",
                        6 => "LEGENDS SAY THE KING VENTURED DEEP, INTO THE DEEPEST DEPTHS FROM WHERE NOTHING RETURNS",
                        7 => "DEATH IS ONLY NATURAL, TO SURVIVE ONE NEEDS WILLPOWER",
                        8 => "DEEPER AND DEEPER AND DEEPER... HOW DEEP SHALL YOU GO?",
                        9 => "THERE ARE STILL DEPTHS LEFT UNEXPLORED",
                        10 => "THE KING MUST BE DEEPER, BUT YOU WON'T LIKE WHAT YOU'LL FIND",
                        _ => unreachable!()
                    }
                );
            }
            char_timer+=1;
            while char_timer>=48 {
                char_timer-=24;
            }
            engine.print(53, 35, "<R> - RETURN");
        }
        else {
            let i1=l.player.get_inv_name(0);
            let i2=l.player.get_inv_name(1);
            let i3=l.player.get_inv_name(2);
            engine.print(0, 0, &l.to_string());
            engine.print(53, 20, "<W> - MOVE FORWARDS");
            engine.print(53, 21, "<A> - MOVE LEFT");
            engine.print(53, 22, "<S> - MOVE BACKWARDS");
            engine.print(53, 23, "<D> - MOVE RIGHT");
            engine.print(53, 24, "<E> - MOVE UP");
            engine.print(53, 25, "<Q> - MOVE DOWN");
            let mut ind=26;
            if i1.is_some() {engine.print(53, ind, &format!("<7> - {}",i1.unwrap())); ind+=1;}
            if i2.is_some() {engine.print(53, ind, &format!("<8> - {}",i2.unwrap())); ind+=1;}
            if i3.is_some() {engine.print(53, ind, &format!("<9> - {}",i3.unwrap())); ind+=1;}
            if l.player.starving() {
                consume_timer+=1;
                consume_timer%=20;
                if consume_timer<10 {
                    engine.print(53, ind, "<C> - CONSUME SELF");
                }
            }
        }

        if in_game {
            if let Some(str) = get_buf() {
                add_to_queue(&mut log_queue, str);
            }
            engine.print(0, 30, &log.clone().join("\n"));
            buf_timer+=1;
            if log_queue.len()>4 {
                buf_timer%=2;
            }
            else if log_queue.len()>=2 {
                buf_timer%=4;
            }
            else if log_queue.len()==1 {
                buf_timer%=8;
            }
            else {
                buf_timer%=10;
            }
            if buf_timer==0 {
                match log_queue.pop_front() {
                    Some(val) => add_to_log(&mut log, val),
                    None => add_to_log(&mut log, String::new()),
                }
                
            }
        }

        engine.draw(); // draw the screen
    }
}