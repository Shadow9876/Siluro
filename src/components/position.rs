pub struct Pos2 {
    pub x: usize,
    pub y: usize
}
impl Pos2 {
    pub fn new(x: usize, y: usize) -> Self {
        Self { x: (x), y: (y) }
    }
}

#[derive(Debug,Clone,Copy)]
pub struct Pos3 {
    pub x: usize,
    pub y: usize,
    pub z: usize
}
impl Pos3 {
    pub fn new(x: usize, y: usize, z: usize) -> Self {
        Self { x: (x), y: (y), z: (z) }
    }
}
