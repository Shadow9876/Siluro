use std::collections::VecDeque;

use rand::Rng;
use super::item::{Food, Item, Weapon};
use super::position::Pos3;

pub static mut BUF: VecDeque<String>=VecDeque::new();

pub fn add_to_buf(s: String) {
    unsafe {BUF.push_back(s.to_uppercase())}
}
pub fn get_buf() -> Option<String> {
    unsafe {BUF.pop_front()}
}

#[macro_export]
macro_rules! player_log {
    ($x:expr) => {
        add_to_buf($x.to_string())
    };
    ($x:expr,$p:expr) => {
        add_to_buf(format!("{}...",$x));
        match $p {
            0 => {
                let messages=["And it was destroyed","And it was shattered"];
                let r=random_value(0,messages.len() as u8) as usize;
                add_to_buf(messages[r].to_string());
            },
            1 => {
                let messages=["And it was greatly damaged","And it was almost destroyed"];
                let r=random_value(0,messages.len() as u8) as usize;
                add_to_buf(messages[r].to_string());
            },
            _ => {}
        }
    };
    (blood $x:expr, $p:expr) => {
        add_to_buf(format!("{}...",$x));
        match $p {
            0 => {
                add_to_buf(String::from("Carp is sleeping"));
            },
            1..=19 => {
                add_to_buf(String::from("Carp is feeling exhausted"));
            },
            20..=39 => {
                add_to_buf(String::from("Carp is feeling sluggish"));
            }
            40..=59 => {
                add_to_buf(String::from("Carp is feeling sleepy"));
            }
            60..=79 => {
                add_to_buf(String::from("Carp is feeling tired"));
            }
            _ => {}
        }
    }
}

#[derive(Debug)]
pub struct Player {
    pub health: u8,
    pub hunger: u8,
    pub mind: u8,
    pub body_parts: BodyParts,
    pub inventory: [Option<Item>;3],
    pub position: Pos3,
    pub death_cause: Option<DeathCause>,
    pub teeth: Weapon
}
impl Player {
    pub fn new() -> Self {
        Self {
            health: (150),
            hunger: (100),
            mind: (100),
            body_parts: (BodyParts::new()),
            inventory: (Default::default()),
            position: (Pos3::new(20, 3, 10)),
            death_cause: (None),
            teeth: (Weapon::new_teeth(50))
        }
    }
    pub fn can_eat(&self) -> bool {
        self.body_parts.face.teeth>0
    }
    pub fn can_see(&self) -> bool {
        self.body_parts.face.eyes.0>0 || self.body_parts.face.eyes.1>0
    }
    pub fn can_see_well(&self) -> bool {
        self.body_parts.face.eyes.0>=5 && self.body_parts.face.eyes.1>=5
    }
    pub fn can_see_perfectly(&self) -> bool {
        self.body_parts.face.eyes.0>=8 && self.body_parts.face.eyes.1>=8
    }
    pub fn depth(&self) -> usize {
        self.position.y
    }
    pub fn is_brain_normal(&self) -> bool {
        self.body_parts.face.brain>=7
    }
    pub fn is_brain_functional(&self) -> bool {
        self.body_parts.face.brain>=2
    }
    pub fn can_go_up(&self) -> bool {
        self.body_parts.internal.swim_bladder>=5
    }
    pub fn can_go_down(&self) -> bool {
        self.body_parts.internal.swim_bladder>0
    }
    pub fn has_spleen(&self) -> bool {
        self.body_parts.internal.spleen!=0
    }
    pub fn are_fins_functional(&self) -> bool {
        let vals=[7,7,7,7,17,12,20u8];
        let mut res=true;
        for (&fin,val) in self.body_parts.torso.fins.iter().zip(vals) {
            res=res&&fin>val
        }
        res
    }
    pub fn damage(&mut self, mut amount: u8, critical: bool) {
        let mut d=medium_damage(amount);
        if self.are_fins_functional() {
            let r=random_value(0, 100);
            if r<2 { player_log!("Carp avoided the damage"); amount=0; }
            else if r<4 { player_log!("Carp dodged part of the attack"); amount/=3; }
            else if r<7 { player_log!("Carp partly managed to get out of the way"); amount/=2; }
            d=small_damage(amount);
        }
        if amount==0 { return; }
        self.set_health(damage_component(self.health, d));
        if critical { player_log!("Carp's sanity decreased"); self.set_mind(damage_component(self.mind, d)); }
        // Check if damage causes fatal injuries
        if self.body_parts.damage(amount) { self.set_health(0) }
    }
    fn get_cause_of_death(&self) -> DeathCause {
        if self.hunger==0 { player_log!("Carp died because of malnutrition"); Hunger }
        else if self.mind==0 { player_log!("Carp went insane"); Mind }
        else if self.body_parts.face.brain==0 { player_log!("Carp suffered irreparable brain damage"); Brain }
        else if self.body_parts.face.gills==0 { player_log!("Carp drowned"); Gills }
        else if self.body_parts.torso.skeleton==0 { player_log!("Carp's skeleton was crushed by pressure"); Skeleton }
        else if self.body_parts.torso.flesh==0 { player_log!("Carp died because of flesh loss"); Flesh }
        else if self.body_parts.torso.blood==0 { player_log!("Carp died because of blood loss"); Blood }
        else if self.body_parts.torso.skin==0 { player_log!("Carp's skin was torn asunder"); Skin }
        else if self.body_parts.internal.hearth==0 { player_log!("Carp's hearth exploded"); Hearth }
        else if self.body_parts.internal.liver==0 { player_log!("Carp's liver was destroyed resulting in a slow and painful death"); Liver }
        else if self.body_parts.internal.kidneys==0 { player_log!("Carp's kidneys were destroyed resulting in a painful, prolonged death"); Kidneys }
        else if self.body_parts.internal.pancreas==0 { player_log!("Carp's pancreas was destroyed. Carp was eaten from the inside"); Pancreas }
        else if self.body_parts.internal.intestine==0 { player_log!("Carp's intestines were destroyed"); Intestine }
        else if self.body_parts.internal.stomach==0 { player_log!("Carp's stomach was destroyed"); Stomach }
        else { player_log!("Carp died because of its cumulative injuries"); Health }
    }
    fn restore_mind(&mut self, amount: u8, within_limit: bool) {
        match within_limit {
            true => {
                self.set_mind(u8::min(self.mind+amount,((self.mind+amount)/20 + 1)*20))
            }
            false => {
                self.set_mind(self.mind+amount)
            }
        }
    }
    fn restore_health(&mut self, amount: u8, within_limit: bool) {
        match within_limit {
            true => {
                self.set_health(u8::min(self.health+amount,((self.health+amount)/30 + 1)*30))
            }
            false => {
                self.set_mind(self.health+amount)
            }
        }
    }
    pub fn rest(&mut self) {
        player_log!("Carp decided to rest for a while");
        self.restore_mind(40, false);
        self.restore_health(60, false);
        self.body_parts.face.set_gills(self.body_parts.face.gills+5);
        self.body_parts.face.set_teeth(self.body_parts.face.teeth+15);
        if self.has_spleen() {
            self.body_parts.internal.set_hearth(self.body_parts.internal.hearth+2);
        }
        self.body_parts.internal.set_intestine(self.body_parts.internal.intestine+4);
        self.body_parts.internal.set_kidneys(self.body_parts.internal.kidneys+2);
        self.body_parts.internal.set_liver(self.body_parts.internal.liver+7);
        self.body_parts.internal.set_pancreas(self.body_parts.internal.pancreas+3);
        if self.body_parts.internal.spleen!=0 {
            self.body_parts.internal.set_spleen(self.body_parts.internal.spleen+2);
        }
        self.body_parts.internal.set_stomach(self.body_parts.internal.stomach+5);
        if self.body_parts.internal.swim_bladder>4 {
            self.body_parts.internal.set_swim_bladder(self.body_parts.internal.swim_bladder+1);
        }
        self.body_parts.torso.set_blood(self.body_parts.torso.blood+if self.has_spleen() {25} else {10});
        self.body_parts.torso.set_flesh(self.body_parts.torso.flesh+10);
        self.body_parts.torso.set_skin(self.body_parts.torso.skin+5);
        for i in 0..self.body_parts.torso.fins.len() {
            if self.body_parts.torso.fins[i]!=0 {
                self.body_parts.torso.set_fin(i, self.body_parts.torso.fins[i]+2);
            }
        }

        self.set_hunger(damage_component(self.hunger, 30));
    }
    pub fn heal(&mut self, amount: u8, critical: bool) {
        player_log!(if !critical {"Carp was suddenly healed"} else {"Carp was miraculously healed"});
        self.restore_mind(if critical {amount} else {amount/2}, critical);
        self.restore_health(if critical {amount*2} else {amount}, critical);
        self.body_parts.face.set_gills(self.body_parts.face.gills+amount/2);
        self.body_parts.face.set_teeth(self.body_parts.face.teeth+amount*2);
        self.body_parts.face.set_brain(self.body_parts.face.brain+1);
        if self.body_parts.face.eyes.0!=0 {
            self.body_parts.face.set_eye_0(self.body_parts.face.eyes.0+amount/10);
        }
        if self.body_parts.face.eyes.1!=0 {
            self.body_parts.face.set_eye_1(self.body_parts.face.eyes.1+amount/10);
        }
        if self.has_spleen() {
            self.body_parts.internal.set_hearth(self.body_parts.internal.hearth+amount/5);
        }
        self.body_parts.internal.set_intestine(self.body_parts.internal.intestine+amount/3);
        self.body_parts.internal.set_kidneys(self.body_parts.internal.kidneys+amount/5);
        self.body_parts.internal.set_liver(self.body_parts.internal.liver+amount/2);
        self.body_parts.internal.set_pancreas(self.body_parts.internal.pancreas+amount/3);
        if self.body_parts.internal.spleen!=0 {
            self.body_parts.internal.set_spleen(self.body_parts.internal.spleen+amount/5);
        }
        self.body_parts.internal.set_stomach(self.body_parts.internal.stomach+amount/2);
        if self.body_parts.internal.swim_bladder>4 {
            self.body_parts.internal.set_swim_bladder(self.body_parts.internal.swim_bladder+amount/10);
        }
        self.body_parts.internal.set_hearth(self.body_parts.internal.hearth+amount/4);
        self.body_parts.torso.set_blood(self.body_parts.torso.blood+if self.has_spleen() {amount*3} else {amount});
        self.body_parts.torso.set_flesh(self.body_parts.torso.flesh+amount);
        self.body_parts.torso.set_skin(self.body_parts.torso.skin+amount/2);
        self.body_parts.torso.set_skeleton(self.body_parts.torso.skeleton+amount/4);
        for i in 0..self.body_parts.torso.fins.len() {
            if self.body_parts.torso.fins[i]!=0 {
                self.body_parts.torso.set_fin(i, self.body_parts.torso.fins[i]+amount/5);
            }
        }
    }
    pub fn eat(&mut self, item: Food, eat_self: bool) {
        let mut n=
        match item {
            Food::Insect(n) => {player_log!("Carp ate the insect"); n},
            Food::Worm(n) => {player_log!("Carp ate the worm"); n},
            Food::Crayfish(n) => {if !eat_self {player_log!("Carp ate the crayfish")} else {player_log!("Carp ate itself")}; n*2},
            Food::Snail(n) => {player_log!("Carp ate the snail"); n*2},
            Food::Berry(n) => {player_log!("Carp ate the berries"); n*2},
            Food::Seed(n) => {player_log!("Carp ate the seeds"); n},
            Food::Bread(n) => {player_log!("Carp ate the delicious bread"); n*3},
        };
        if self.body_parts.internal.stomach<12 || self.body_parts.internal.intestine<10 || self.body_parts.internal.pancreas<10 {
            n/=2;
            player_log!("But it couldn't properly digest all of it");
        }
        self.set_hunger(self.hunger+n);
    }
    fn set_health(&mut self, val: u8) {
        self.health=u8::min(val,150);
        if self.hunger<30 {
            player_log!("CARP IS ALMOST DEAD");
        }
        else if self.hunger<60 {
            player_log!("CARP IS GRAVELY WOUNDED");
        }
        else if self.hunger<90 {
            player_log!("CARP IS WOUNDED");
        }
        else if self.hunger<120 {
            player_log!("CARP IS INJURED");
        }
        if self.health==0 {self.death_cause=Some(self.get_cause_of_death())}
    }
    pub fn starving(&self) -> bool {
        self.hunger<20
    }
    fn set_hunger(&mut self, val: u8) {
        self.hunger=u8::min(val,100);
        if self.hunger<20 {
            player_log!("CARP IS SUFFERING FROM MALNUTRITION");
        }
        else if self.hunger<40 {
            player_log!("CARP IS STARVING");
        }
        else if self.hunger<60 {
            player_log!("CARP IS HUNGRY");
        }
        else if self.hunger<80 {
            player_log!("CARP IS FEELING PECKISH");
        }

        if self.hunger==0 {self.death_cause=Some(self.get_cause_of_death())}
    }
    fn set_mind(&mut self, val: u8) {
        self.mind=u8::min(val,100);        
        if self.mind<20 {
            player_log!("CARP IS GOING INSANE");
        }
        else if self.mind<40 {
            player_log!("CARP IS NOT OKAY");
        }
        else if self.mind<60 {
            player_log!("CARP IS HALLUCINATING");
        }
        if self.mind==0 {self.death_cause=Some(self.get_cause_of_death())}
    }
    pub fn eat_self(&mut self) {
        player_log!("Carp took a bite out of itself");
        self.eat(Food::Crayfish(35),true);
        self.set_mind(damage_component(self.mind, 30));
        if self.body_parts.torso.damage(35,true) { self.set_health(0) }
    }
    pub fn is_alive(&self) -> bool {
        self.death_cause.is_none()
    }
    pub fn cause_damage(&mut self) -> u8 {
        for i in 0..self.inventory.len() {
            match &self.inventory[i] {
                Some(val) => {
                    match val {
                        Item::Weapon(w) => {
                            match w {
                                Weapon::Stick { durability, .. } => {
                                    if *durability==0 {
                                        player_log!("KNIFE BROKE");
                                        self.inventory[i]=None;
                                    }
                                },
                                Weapon::Knife { durability, .. } => {
                                    if *durability==0 {
                                        player_log!("STICK BROKE");
                                        self.inventory[i]=None;
                                    }
                                },
                                Weapon::Teeth { .. } => {},
                            }
                        }
                        _ => {}
                    }
                },
                None => {},
            }
        }
        let mut weapon=&mut self.teeth;
        for item in &mut self.inventory {
            match item {
                Some(val) => {
                    match val {
                        Item::Weapon(w) => weapon=w,
                        _ => {}
                    }
                },
                None => {}
            }
        }
        weapon.deal_damage()
    }
    pub fn use_item(&mut self, item: Item) -> bool {
        match item {
            Item::Food(f) => {self.eat(f,false); true},
            Item::Potion(p) => {
                match p {
                    super::item::Potion::LesserHealing(val) => self.heal(val, false),
                    super::item::Potion::Healing(val) => self.heal(val, false),
                    super::item::Potion::GreaterHealing(val) => self.heal(val, true),
                    super::item::Potion::LesserSanity(val) => self.restore_mind(val, true),
                    super::item::Potion::Sanity(val) => self.restore_mind(val, false),
                    super::item::Potion::Masochism(val) => {
                        self.damage(val, false);
                        self.teeth.increase_damage(val/3);
                    },
                }
                true
            },
            Item::Weapon(_) => false,
        }
    }
    pub fn stash_item(&mut self, item: Item) -> bool {
        for i in 0..self.inventory.len() {
            if matches!(self.inventory[i],None) {
                self.inventory[i]=Some(item);
                return true;
            }
        }
        return false;
    }
    pub fn use_item_from_inventory(&mut self, i: usize) -> bool {
        if let Some(val)=&self.inventory[i] {
            self.use_item(val.clone())
        }
        else {false}
    }
    pub fn get_inv_name(&self, i: usize) -> Option<String> {
        if let Some(val)=&self.inventory[i] {
            match val {
                Item::Food(f) => {
                    match f {
                        Food::Insect(_) => Some(String::from("EAT INSECT")),
                        Food::Worm(_) => Some(String::from("EAT WORM")),
                        Food::Crayfish(_) => Some(String::from("EAT CRAYFISH")),
                        Food::Snail(_) => Some(String::from("EAT SNAIL")),
                        Food::Berry(_) => Some(String::from("EAT BERRIES")),
                        Food::Seed(_) => Some(String::from("EAT SEEDS")),
                        Food::Bread(_) => Some(String::from("EAT BREAD")),
                    }
                },
                Item::Weapon(w) => {
                    match w {
                        Weapon::Stick { damage, .. } => Some(format!("DROP STICK ({damage})")),
                        Weapon::Knife { damage, .. } => Some(format!("DROP KNIFE ({damage})")),
                        Weapon::Teeth { .. } => unreachable!()
                    }
                },
                Item::Potion(p) => {
                    match p {
                        super::item::Potion::LesserHealing(_) => Some(String::from("DRINK RED LIQUID")),
                        super::item::Potion::Healing(_) => Some(String::from("DRINK VELVET LIQUID")),
                        super::item::Potion::GreaterHealing(_) => Some(String::from("DRINK PINK LIQUID")),
                        super::item::Potion::LesserSanity(_) => Some(String::from("DRINK BLUE LIQUID")),
                        super::item::Potion::Sanity(_) => Some(String::from("DRINK WHITE LIQUID")),
                        super::item::Potion::Masochism(_) => Some(String::from("DRINK BLACK LIQUID")),
                    }
                },
            }
        }
        else { None }
    } 
    pub fn decrease_hunger(&mut self) {
        if random_value(0, 10)<1 {
            self.set_hunger(self.hunger-1);
        }
    }
}

pub fn damage_component(mut component: u8, amount: u8) -> u8 {
    if amount<component { component-=amount; }
    else { component=0; }
    component
}

fn small_damage(amount: u8) -> u8 {
    amount/2
}
fn medium_damage(amount: u8) -> u8 {
    amount
}
fn high_damage(amount: u8) -> u8 {
    amount*2
}

pub fn random_value(start: u8, end: u8) -> u8 {
    let mut rng = rand::thread_rng();
    rng.gen_range(start..end)
}

#[derive(Debug)]
pub struct BodyParts {
    pub face: FaceParts,
    pub torso: TorsoParts,
    pub internal: Organs,
}
impl BodyParts {
    pub fn new() -> Self {
        Self { face: (FaceParts::new()), torso: (TorsoParts::new()), internal: (Organs::new()) }
    }
    pub fn damage(&mut self, amount: u8) -> bool {
        let r=random_value(0, 100);
        if r<20 {
            self.internal.damage(amount)
        }
        else if r<45 {
            self.face.damage(amount)
        }
        else {
            self.torso.damage(amount,false)
        }
    }
}

#[derive(Debug)]
pub struct FaceParts {
    pub gills: u8,
    pub eyes: (u8,u8),
    pub teeth: u8,
    pub brain: u8,
}
impl FaceParts {
    pub fn new() -> Self {
        Self { gills: (15), eyes: ((10,10)), teeth: (50), brain: (10) }
    }
    pub fn damage(&mut self, amount: u8) -> bool {
        let r=random_value(0, 100);
        if r<10 {
            self.brain=damage_component(self.brain, small_damage(amount));
            let val= 
            if self.brain==0 {0}
            else if self.brain<4 {1}
            else {2};
            player_log!("Carp's brain was damaged",val);
            self.brain==0
        }
        else if r<20 {
            self.gills=damage_component(self.gills, small_damage(amount));
            let val= 
            if self.gills==0 {0}
            else if self.gills<6 {1}
            else {2};
            player_log!("Carp's gills were damaged",val);
            self.gills==0
        }
        else if r<50 {
            if self.eyes.0==0 {
                self.damage_teeth(amount)
            }
            else {
                self.eyes.0=damage_component(self.eyes.0, small_damage(amount));
                let val= 
                if self.eyes.0==0 {0}
                else if self.eyes.0<4 {1}
                else {2};
                player_log!("Carp's left eye was damaged",val);
                false
            }
        }
        else if r<70 {
            if self.eyes.1==0 {
                self.damage_teeth(amount)
            }
            else {
                self.eyes.1=damage_component(self.eyes.1, small_damage(amount));
                let val= 
                if self.eyes.1==0 {0}
                else if self.eyes.1<4 {1}
                else {2};
                player_log!("Carp's right eye was damaged",val);
                false
            }
        }
        else {
            self.damage_teeth(amount)
        }
    }
    fn damage_teeth(&mut self, amount: u8) -> bool {
        self.teeth=damage_component(self.teeth, medium_damage(amount));
        let val= 
        if self.teeth==0 {0}
        else if self.teeth<10 {1}
        else {2};
        player_log!("Carp's teeth were damaged",val);
        false
    }
    fn set_gills(&mut self, val: u8) {
        self.gills=u8::min(val,15)
    }
    fn set_eye_0(&mut self, val: u8) {
        self.eyes.0=u8::min(val,10)
    }
    fn set_eye_1(&mut self, val: u8) {
        self.eyes.1=u8::min(val,10)
    }
    fn set_teeth(&mut self, val: u8) {
        self.teeth=u8::min(val,50)
    }
    fn set_brain(&mut self, val: u8) {
        self.brain=u8::min(val,10)
    }
}

#[derive(Debug)]
pub struct TorsoParts {
    pub fins: [u8;7],
    pub skeleton: u8,
    pub skin: u8,
    pub flesh: u8,
    pub blood: u8
}
impl TorsoParts {
    pub fn new() -> Self {
        Self { fins: ([20,20,20,20,35,25,50]), skeleton: (50), skin: (25), flesh: (80), blood: (100) }
    }
    pub fn damage(&mut self, amount: u8, damage_only_edible: bool) -> bool {
        let mut r=random_value(0, 100);
        // Damage to flesh and skeleton
        if r<45 {
            r=random_value(0, 100);
            if r<40 && !damage_only_edible {
                self.skeleton=damage_component(self.skeleton, medium_damage(amount));
                let val= 
                if self.skeleton==0 {0}
                else if self.skeleton<13 {1}
                else {2};
                player_log!("Carp's skeleton was damaged",val);
                self.blood=damage_component(self.blood, small_damage(amount));
                player_log!(blood "Carp lost some blood",self.blood);
                self.skeleton==0 || self.blood==0
            }
            else {
                self.flesh=damage_component(self.flesh, medium_damage(amount));
                let val= 
                if self.flesh==0 {0}
                else if self.flesh<20 {1}
                else {2};
                player_log!("Carp's flesh was damaged",val);
                self.blood=damage_component(self.blood, small_damage(amount));
                player_log!(blood "Carp lost some blood",self.blood);
                self.flesh==0 || self.blood==0
            }
        }
        // Damage to skin and fins
        else {
            r=random_value(0, 100);
            if r<70 || damage_only_edible {
                r=random_value(0, 100);
                if r<84 {
                    let idx=(r/14) as usize;
                    self.fins[idx]=damage_component(self.fins[idx], medium_damage(amount));
                    let val=
                    if self.fins[idx]==0 {0}
                    else if self.fins[idx]<6 {1}
                    else {2};
                    player_log!("One of Carp's fins was damaged",val);
                }
                else {
                    self.fins[6]=damage_component(self.fins[6], high_damage(amount));
                    let val=
                    if self.fins[6]==0 {0}
                    else if self.fins[6]<10 {1}
                    else {2};
                    player_log!("Carp's back fin was damaged",val);
                }
                self.blood=damage_component(self.blood, small_damage(amount));
                player_log!(blood "Carp lost some blood",self.blood);
                self.blood==0
            }
            else {
                self.skin=damage_component(self.skin, small_damage(amount));
                let val=
                if self.skin==0 {0}
                else if self.skin<8 {1}
                else {2};
                player_log!("Carp's skin was damaged",val);
                self.blood=damage_component(self.blood, medium_damage(amount));
                player_log!(blood "Carp lost a lot of blood",self.blood);
                self.skin==0 || self.blood==0
            }
        }
    }
    pub fn set_fin(&mut self, i: usize, val: u8) {
        self.fins[i]=u8::min(val,[20,20,20,20,35,25,50][i])
    }
    pub fn set_skeleton(&mut self, val: u8) {
        self.skeleton=u8::min(val,50);
    }
    pub fn set_skin(&mut self, val: u8) {
        self.skeleton=u8::min(val,25);
    }
    pub fn set_flesh(&mut self, val: u8) {
        self.skeleton=u8::min(val,80);
    }
    pub fn set_blood(&mut self, val: u8) {
        self.skeleton=u8::min(val,100);
    }
}

#[derive(Debug)]
pub struct Organs {
    pub hearth: u8,
    pub liver: u8,
    pub spleen: u8,
    pub pancreas: u8,
    pub intestine: u8,
    pub stomach: u8,
    pub kidneys: u8,
    pub swim_bladder: u8
}
impl Organs {
    pub fn new() -> Self {
        Self { hearth: (15), liver: (25), spleen: (20), swim_bladder: (10), pancreas: (30), intestine: (25), stomach: (35), kidneys: (25) }
    }
    pub fn damage(&mut self, amount: u8) -> bool {
        let r=random_value(0, 100);
        if r<10 {
            self.hearth=damage_component(self.hearth, small_damage(amount));
            let val=
            if self.hearth==0 {0}
            else if self.hearth<5 {1}
            else {2};
            player_log!("Carp's hearth was damaged",val);
            self.hearth==0
        }
        else if r<20 {
            self.swim_bladder=damage_component(self.swim_bladder, small_damage(amount));
            let val=
            if self.swim_bladder==0 {0}
            else if self.swim_bladder<5 {1}
            else {2};
            player_log!("Carp's swim bladder was damaged",val);
            false
        }
        else if r<30 {
            self.liver=damage_component(self.liver, medium_damage(amount));
            let val=
            if self.liver==0 {0}
            else if self.liver<8 {1}
            else {2};
            player_log!("Carp's liver was damaged",val);
            self.liver==0
        }
        else if r<50 {
            self.spleen=damage_component(self.spleen, medium_damage(amount));
            let val=
            if self.hearth==0 {0}
            else if self.hearth<6 {1}
            else {2};
            player_log!("Carp's spleen was damaged",val);
            false
        }
        else if r<60 {
            self.kidneys=damage_component(self.kidneys, medium_damage(amount));
            let val=
            if self.kidneys==0 {0}
            else if self.kidneys<8 {1}
            else {2};
            player_log!("Carp's kidneys were damaged",val);
            self.kidneys==0
        }
        else if r<70 {
            self.pancreas=damage_component(self.pancreas, medium_damage(amount));
            let val=
            if self.pancreas==0 {0}
            else if self.pancreas<9 {1}
            else {2};
            player_log!("Carp's pancreas was damaged",val);
            self.pancreas==0
        }
        else if r<80 {
            self.intestine=damage_component(self.intestine, medium_damage(amount));
            let val=
            if self.intestine==0 {0}
            else if self.intestine<8 {1}
            else {2};
            player_log!("Carp's intestines were damaged",val);
            self.intestine==0
        }
        else {
            self.stomach=damage_component(self.stomach, medium_damage(amount));
            let val=
            if self.stomach==0 {0}
            else if self.stomach<11 {1}
            else {2};
            player_log!("Carp's stomach was damaged",val);
            self.stomach==0
        }
    }
    pub fn set_hearth(&mut self, val: u8) {
        self.hearth=u8::min(val,15);
    }
    pub fn set_liver(&mut self, val: u8) {
        self.liver=u8::min(val,25);
    }
    pub fn set_spleen(&mut self, val: u8) {
        self.hearth=u8::min(val,20);
    }
    pub fn set_swim_bladder(&mut self, val: u8) {
        self.swim_bladder=u8::min(val,10);
    }
    pub fn set_pancreas(&mut self, val: u8) {
        self.pancreas=u8::min(val,30);
    }
    pub fn set_intestine(&mut self, val: u8) {
        self.intestine=u8::min(val,25);
    }
    pub fn set_stomach(&mut self, val: u8) {
        self.stomach=u8::min(val,35);
    }
    pub fn set_kidneys(&mut self, val: u8) {
        self.kidneys=u8::min(val,25);
    }
}


#[derive(Debug,Hash,PartialEq,Eq,Clone, Copy)]
pub enum DeathCause {
    Health,
    Hunger,
    Mind,
    Brain,
    Gills,
    Skeleton,
    Flesh,
    Blood,
    Skin,
    Hearth,
    Liver,
    Kidneys,
    Pancreas,
    Intestine,
    Stomach
}
use DeathCause::*;
