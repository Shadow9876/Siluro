#[derive(Debug, Clone, Copy)]
pub enum Trap {
    Net,
    Predator,
    SafeHouse
}