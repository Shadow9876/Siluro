use super::player::random_value;

#[derive(Debug,Clone)]
pub enum Item {
    Food(Food),
    Weapon(Weapon),
    Potion(Potion),
    //Utility(Utility)
}

#[derive(Debug,Clone)]
pub enum Food {
    Insect(u8),
    Worm(u8),
    Crayfish(u8),
    Snail(u8),
    Berry(u8),
    Seed(u8),
    Bread(u8)
}
impl Food {
    pub fn new(depth: u8) -> Self {
        let r=random_value(0, 100);
        // Bread, berry, seed, insect
        if depth<3 {
            match r {
                0..=24 => Food::Berry(45-depth),
                25..=49 => Food::Seed(38-depth),
                50..=74 => Food::Insect(42-depth),
                _ => Food::Bread(50-depth)
            }
        }
        // Insect, worm, snail, seed
        else if depth<5 {
            match r {
                0..=19 => Food::Snail(29-depth),
                20..=39 => Food::Worm(28-depth),
                40..=69 => Food::Seed(35-depth),
                _ => Food::Insect(40-depth)
            }

        }
        // Worm, snail, crayfish
        else if depth<9 {
            match r {
                0..=19 => Food::Crayfish(30-depth),
                20..=69 => Food::Snail(27-depth),
                _ => Food::Worm(25-depth)
            }
        }
        // Snail
        else {
            Food::Snail(20-depth)
        }
    }
}

#[derive(Debug,Clone)]
pub enum Potion {
    LesserHealing(u8),
    Healing(u8),
    GreaterHealing(u8),
    LesserSanity(u8),
    Sanity(u8),
    // Turns health into damage
    Masochism(u8)
}
impl Potion {
    pub fn new_healing(depth: u8) -> Self {
        let r=random_value(0, 100);
        if depth<5 {
            match r {
                0..=59 => Potion::LesserHealing(random_value(15, 25)),
                _ => Potion::Healing(random_value(20, 31))
            }
        }
        else if depth<9 {
            match r {
                0..=39 => Potion::Healing(random_value(20, 35)),
                40..=79 => Potion::Healing(random_value(30, 41)),
                _ => Potion::GreaterHealing(random_value(50, 61))
            }
        }
        else {
            match r {
                0..=39 => Potion::Healing(random_value(35, 46)),
                _ => Potion::GreaterHealing(random_value(60, 76))
            }
        }
    }
    pub fn new_sanity(depth: u8) -> Self {
        let r=random_value(0, 100);
        if depth<5 {
            match r {
                0..=59 => Potion::LesserSanity(random_value(10, 18)),
                _ => Potion::LesserSanity(random_value(15, 26))
            }
        }
        else if depth<9 {
            match r {
                0..=39 => Potion::LesserSanity(random_value(12, 21)),
                40..=79 => Potion::LesserSanity(random_value(17, 29)),
                _ => Potion::Sanity(random_value(20, 31))
            }
        }
        else {
            match r {
                0..=39 => Potion::LesserSanity(random_value(20, 31)),
                _ => Potion::Sanity(random_value(27, 41))
            }
        }
    }
}

#[derive(Debug,Clone)]
pub enum Weapon {
    Teeth {health: u8, damage: u8},
    Stick {durability: u8, damage: u8},
    Knife {durability: u8, damage: u8},
}
impl Weapon {
    pub fn increase_damage(&mut self, val: u8) {
        match self {
            Weapon::Teeth { damage, ..} => *damage+=val,
            Weapon::Stick { damage, ..} => *damage+=val,
            Weapon::Knife { damage, ..} => *damage+=val,
        }
    }
    pub fn new_teeth(val: u8) -> Self {
        Self::Teeth { health: (val), damage: (random_value(5, 8)) }
    }
    pub fn new_stick() -> Self {
        Self::Stick { durability: (random_value(12, 25)), damage: (random_value(6, 10)) }
    }
    pub fn new_knife() -> Self {
        Self::Knife { durability: (random_value(30, 46)), damage: (random_value(8, 13)) }
    }
    pub fn get_damage(&self) -> u8 {
        match self {
            Weapon::Teeth { damage, .. } => *damage,
            Weapon::Stick { damage, .. } => *damage,
            Weapon::Knife { damage, .. } => *damage,
        }
    }
    pub fn deal_damage(&mut self) -> u8 {
        let r=random_value(0, 100);
        match self {
            // Average teeth.damage: 6
            // Maximum teeth.damage: 7
            // Minimum teeth.damage: 5
            // Average damage: 7.8
            // Maximum damage: 14
            // Minimum damage: 5
            Weapon::Teeth { damage, health } => {
                if r<70 {
                    (*damage as i32**health as i32/50) as u8
                }
                else {
                    (*damage as i32*2**health as i32/50) as u8
                }
            },
            // Average stick.damage: 7.5
            // Maximum stick.damage: 9
            // Minimum stick.damage: 6
            // Average damage: 10.5
            // Maximum damage: 18
            // Minimum damage: 6
            Weapon::Stick { durability, damage } => {
                if r<60 {
                    *durability-=1;
                    *damage
                }
                else if r<80 {
                    *durability-=1;
                    *damage*2
                }
                else {
                    *damage*2
                }
            },
            // Average knife.damage: 10
            // Maximum knife.damage: 12
            // Minimum knife.damage: 8
            // Average damage: 13
            // Maximum damage: 36
            // Minimum damage: 8
            Weapon::Knife { durability, damage } => {
                if r<80 {
                    *durability-=1;
                    *damage
                }
                else if r<90 {
                    *durability-=1;
                    *damage*2
                }
                else {
                    *damage*3
                }
            },
        }
    }
}

/*#[derive(Debug,Clone)]
pub enum Utility {
    Backpack(Vec<Item>),
    Lockpick,
    Armour {protection: u8}
}*/
