use super::player::{damage_component, random_value};
use crate::components::player::add_to_buf;

macro_rules! enemy_log {
    ($x:expr) => {
        add_to_buf(format!("{}!",$x));
        //println!("{}!",$x)
    };
    ($x:expr,$p:expr) => {
        add_to_buf(format!("{}...",$x));
        //println!("{}...",$x);
        match $p {
            0 => {
                let messages=["And it was destroyed","And it came clean off","And it shattered into bloody little pieces","And it was mutiliated beyond recognition"];
                let r=random_value(0,messages.len() as u8) as usize;
                //println!("{}",messages[r]);
                add_to_buf(format!("{}",messages[r]));
            },
            1 => {
                let messages=["And it is ailing","And it is so close to annihilation","And it was greatly damaged"];
                let r=random_value(0,messages.len() as u8) as usize;
                //println!("{}",messages[r]);
                add_to_buf(format!("{}",messages[r]));

            },
            5 => {
                let messages=["And it didn't achieve much","And it barely made a scratch","And it did little harm"];
                let r=random_value(0,messages.len() as u8) as usize;
                //println!("{}",messages[r]);
                add_to_buf(format!("{}",messages[r]));

            },
            _ => {}
        }
    };
}

#[derive(Debug, Clone)]
pub enum Enemy {
    SmallFish(SmallFish),
    MediumFish(MediumFish),
    LargeFish(LargeFish),
    //Pack(FishPack),
    Guard(Guard),
    /*GuardPack(GuardPack),
    DepthGuards(DepthGuards)*/
}
impl Enemy {
    pub fn deal_damage(&self) -> Vec<u8> {
        match self {
            Enemy::SmallFish(f) => f.deal_damage(),
            Enemy::MediumFish(f) => f.deal_damage(),
            Enemy::LargeFish(f) => f.deal_damage(),
            Enemy::Guard(f) => f.deal_damage(),
        }
    }
    pub fn receive_damage(&mut self, amount: u8, part: u8) -> bool {
        match self {
            Enemy::SmallFish(f) => f.receive_damage(amount, part),
            Enemy::MediumFish(f) => f.receive_damage(amount, part),
            Enemy::LargeFish(f) => f.receive_damage(amount, part),
            Enemy::Guard(f) => f.receive_damage(amount, part),
        }
    }
    pub fn retreat(&mut self) -> bool {
        match self {
            Enemy::SmallFish(f) => f.retreat(),
            Enemy::MediumFish(f) => f.retreat(),
            Enemy::LargeFish(f) => f.retreat(),
            Enemy::Guard(f) => f.retreat(),
        }
    }
    pub fn return_limbs(&self) -> Vec<u8> {
        match self {
            Enemy::SmallFish(f) => f.return_limbs(),
            Enemy::MediumFish(f) => f.return_limbs(),
            Enemy::LargeFish(f) => f.return_limbs(),
            Enemy::Guard(f) => f.return_limbs(),
        }
    }
    pub fn print_opponent(&self) -> String {
        match self {
            Enemy::SmallFish(f) => f.print_opponent(),
            Enemy::MediumFish(f) => f.print_opponent(),
            Enemy::LargeFish(f) => f.print_opponent(),
            Enemy::Guard(f) => f.print_opponent(),
        }
    }
}

pub trait EnemyBehaviour {
    fn deal_damage(&self) -> Vec<u8>;
    fn receive_damage(&mut self, amount: u8, part: u8) -> bool;
    fn retreat(&mut self) -> bool;
    fn print_opponent(&self) -> String;
    fn return_limbs(&self) -> Vec<u8>;
}

#[derive(Debug, Clone)]
pub struct SmallFish {
    health: u8,
    damage: u8,
}
impl SmallFish {
    pub fn new() -> Self {
        Self { health: (random_value(5, 9)), damage: (random_value(5, 8)) }
    }
}
impl EnemyBehaviour for SmallFish {
    fn deal_damage(&self) -> Vec<u8> {
        enemy_log!("Small fish tries to attack");
        vec![self.damage]
    }
    fn receive_damage(&mut self, amount: u8, _part: u8) -> bool {
        self.health=damage_component(self.health, amount);
        let val=
        if self.health==0 {
            0
        } else if self.health<4 {
            1
        } else { 4 };
        enemy_log!("Small fish's body was greatly damaged",val);
        self.health==0
    }
    fn retreat(&mut self) -> bool {
        let val=self.health<=2;
        if val {
            enemy_log!("Small fish is retreating");
        }
        val
    }
    fn print_opponent(&self) -> String {
        String::from(
            "

                                                                                                                       
                                                                                                                       
                                                                                                                       
                            @@@@@@@#%@%#%@@%@%*%%@@+===*@                                                              
                     %@%@@@%%#*++%%@+*+-*+*#*%@@@@@@@++=---=-=*@%                                                      
                 #==+=-::-@@%*#%*=-+*=:--==-=-+#=+*%+-----=---::-:--=+%                                                
              #@=%===-=+**#**++==----#=---::--*#@*:###:----+==:::-=---+++*%@                                           
              +=*%@#=----=+****++**@*::-:-###@@%+*-=:-===--*=----:=-=-:-==++#@@                                        
              ===%@%-+#++++*##*##*+#-::=*###*-=*#%-+-*-:+==+*----=::::--:===+=+%@                                      
               *+++@@%#@@##*+++*#@#:-=#*@#**==*:--#%:-=+**-=-==+=+--=---:----==++*@                                    
                %*++%%%%@%@@%#@@@-*+-----:::==-=--%-%#%##:**=*-:-#+++::--------==**@@                                  
                 ##*+++*#%@@@@%+%@%::::-::------::#*%+*+%%##--:----:+==-:-::------===@                                 
                   #*#++++**+==*++-#*========--:%#*%%-#*+=*:----=-:*-==+-::----=@@@@@*#@@                              
                      #*++=+++#+-:::::::::::=#+%#%#*+#%:-*#:-:::-::--*--:++-==-----=*%@*+=-%@                          
                        #***+----:::::::-%%%@#@%%@%#%#%+-+=:-=+-+=:-==-+:+*:-*-*-:=::=##%#**#@                         
                            #+=--+==:::++**###%%@*--%#*%-:=::-#-:::==#*+:--=-=::=:::+-%-----*@@                        
                                =@@#--=:=#**-::::::=#####==-:###+-**+::=*=*+--::::::-:------=%                         
                                +=@#%=-++%##---++:-::*###--=-=##=*=::-:----:-:--:=:------:-:-@                         
                                *+=%#@*#%+*%%**+=--==%=#::-:-:=*=:::--::+=--:::::----:--:-#@@@                         
                                *=*-+#@%#@*##    +##*#*--#:::::::=:::::::::%:::-:::------=#@@                          
                                 *++-++#@#%+%@      #+*==--:::::-:--::::::::--:::--------==%%                          
                                   *+++%            @**=*++-=--------::--::::----------==+=%                           
                                    @*+@                %##++==-===-----------=  ----==+@%                             
                                                              %@#+**#+*=+++*@                                          
                                                                                                                       

                                                                                                                       
                                                                                                                       
                                                                                                                       "
        )
    }
    fn return_limbs(&self) -> Vec<u8> {
        return vec![0];
    }
}

#[derive(Debug, Clone)]
pub struct MediumFish {
    head: u8,
    torso: u8,
    damage: u8,
}
impl MediumFish {
    pub fn new() -> Self {
        Self {
            head: (random_value(7, 11)),
            torso: (random_value(10, 24)),
            damage: (random_value(7, 12))
        }
    }
}
impl EnemyBehaviour for MediumFish {
    fn deal_damage(&self) -> Vec<u8> {
        enemy_log!("Fish attacks with its body");
        vec![self.damage]
    }
    fn receive_damage(&mut self, amount: u8, part: u8) -> bool {
        match part {
            0 => {
                let r=random_value(0, 100);
                if r<98 {
                    self.torso=damage_component(self.torso, amount);
                    let val=
                    if self.torso>12 {
                        5
                    } else if self.torso==0 {
                        0
                    } else if self.torso<5 {
                        1
                    } else { 4 };
                    enemy_log!("Fish's torso was damaged",val);
                    self.torso==0
                }
                else {
                    enemy_log!("Fish avoided the attack");
                    false
                }
            },
            _ => {
                let r=random_value(0, 100);
                if r<30 {
                    self.head=damage_component(self.head, amount);
                    let val=
                    if self.head>7 {
                        5
                    } else if self.head==0 {
                        0
                    } else if self.head<5 {
                        1
                    } else { 4 };
                    enemy_log!("Fish's head was damaged",val);
                    self.head==0
                }
                else {
                    enemy_log!("Fish managed to twist its head out of harm's way");
                    false
                }
            }
        }
    }
    fn retreat(&mut self) -> bool {
        let val=self.head<=4 || self.torso<=3;
        if val {
            enemy_log!("Fish is retreating");
        }
        val
    }
    fn print_opponent(&self) -> String {
        String::from(
"                                                     #*###%@@@@@@@@%%##############*******++++++++++==++*###+          
                                                   *@%%@@%#%%#@@%@%%%%#########********+++++++++++=======+++=          
                                                *%%%#%########%%%%%#%%@%##%##*******++++++++++===============          
                                              *@####%##%#@######%%#%%%#%##**********+++++++++++=++===========          
                                            #%@@%#%%#########%#%%%%%########*#*######*###*#******++++++++==            
                                          %%@@@#%%#######*##%#%%#####*#*#*#***#*************+++++++++====-             
                                        #@@@@@%#######*******###%%#%##*#*#**********+++++++============---             
                                       @@%@%%@@@@@%@@%###****##%#####****#********+*+++++++=+==========-               
                                     *%%%##@@@@%%%%%######*###%#######*********+*++++++++++============                
                                    ##%@%%###%########************#*##*****+***++++++++==============                  
                                  *@@%@@##***#%#%##########*+*****#********+****+++++++==+=========   ===              
                                *%#%@#%#%@@%%*##%#######%%@#+++*+*#****++**+*+*++++++++=+++=======  ==+++=             
                              +@@@@@@###@##%##%####%%%%%%###*+++*%***+++++*+**++++++++====+======   +++++==            
                           %@@@@@@@%%@@%@@@###%%@@@##%%%####**++++++++++++++++++++=+====+=+===== =++**++++==           
                          *%@@@@@@%#%@@@***#%%%%%%%#%#********+++*+*+++++++++++=+=++++++========+++***++++=            
                         +##%%%%%%@@@%@%*==-++*#%#%###%#####**+***=++=+++++==++===++++=++++++++++++*+==-               
                        +@##########++-+-*#=%-+**#*#######*******+=+===+==++=+==++++*++====+++++*+=-                   
                       *@@@%###***##=-=#-:::=++******#*#***+****++**==+++++*+++++*****+++****++=*                      
                      #@%%%@@%#######+-*-::++++****+***********+++*--=+*****************+=-                            
                     @%%#%#%#####%%##==-=**+==*##***++***++***++*+#%%###%%@@@%%%####*+                                 
                    *@*++**%@#%@@%###*+===-=*##*********++++++++++==+++++*#@@@@%%##***++==                             
                    #**%@@%++*%#######***++++***#****####+++*=+++*++++++***#@@@%%###%%@%%##*+                          
                   ##*---=*#**+=-+*******++====+*###***##*+===+******+++****#%%%%%@@@@@%@@@%%%%#+=                     
                   @##%@%+=----++++-=++*****+**++++++**#*++#*+*#*****+++++++**#%%%%%%%%%%%%%%%%#*++=                   
                   *  +*#%#####*=--++=-=*#***#**#*+==*+************##*#****++*+*#%%%%%%%##%%%%####%%%%#=               
                       ****==+===+++=-=++==+***####***++**#******++==+**#####*****#**##**+*###**+=                     
                          ******##***++-+++=+***********###***#*  *###############******+=                             
                              *#####**##*****+**#*########             +*#%%%%####%#%%##*+                             
                                     *#########*"
        )
    }
    fn return_limbs(&self) -> Vec<u8> {
        vec![0,1]
    }
}

#[derive(Debug, Clone)]
pub struct LargeFish {
    head: u8,
    torso: u8,
    tail: u8,
    damage: u8,
    tail_damage: u8
}
impl LargeFish {
    pub fn new() -> Self {
        Self {
            head: (random_value(18, 32)),
            torso: (random_value(30, 47)),
            tail: (random_value(8, 12)),
            damage: (random_value(10, 16)),
            tail_damage: (random_value(18, 33))
        }
    }
}
impl EnemyBehaviour for LargeFish {
    fn deal_damage(&self) -> Vec<u8> {
        let r=random_value(0, 100);
        if r<60 {
            enemy_log!("Large Fish attacks with its body");
            vec![self.damage]
        }
        else {
            if self.tail>0 {
                enemy_log!("Large Fish attacks with its muscular tail");
                vec![self.tail_damage]
            }
            else {
                enemy_log!(["Large Fish is looking at its wounds","Large Fish is afraid"][random_value(0, 2) as usize]);
                vec![]
            }
        }
    }
    fn receive_damage(&mut self, amount: u8, part: u8) -> bool {
        match part {
            0 => {
                let r=random_value(0, 100);
                if r<98 {
                    self.torso=damage_component(self.torso, amount);
                    let val=
                    if self.torso>35 {
                        5
                    } else if self.torso==0 {
                        0
                    } else if self.torso<12 {
                        1
                    } else { 4 };
                    enemy_log!("Large fish's torso was damaged",val);
                    self.torso==0
                }
                else {
                    enemy_log!("Large fish avoided the attack");
                    false
                }
            },
            1 => {
                let r=random_value(0, 100);
                if r<35 {
                    self.head=damage_component(self.head, amount);
                    let val=
                    if self.head>18 {
                        5
                    } else if self.head==0 {
                        0
                    } else if self.head<10 {
                        1
                    } else { 4 };
                    enemy_log!("Large fish's head was damaged",val);
                    self.head==0
                }
                else {
                    enemy_log!("Large fish elegantly moved its head out of the way");
                    false
                }
            }
            _ => {
                let r=random_value(0, 100);
                if r<70 {
                    self.tail=damage_component(self.tail, amount);
                    let val=
                    if self.tail>10 {
                        5
                    } else if self.tail==0 {
                        0
                    } else if self.tail<5 {
                        1
                    } else { 4 };
                    enemy_log!("Large fish's tail was damaged",val);
                }
                else {
                    enemy_log!("Large fish swiftly moved its tail out of harm's way");
                }
                false
            }
        }
    }
    fn retreat(&mut self) -> bool {
        let val=self.head<=7 || self.torso<=9;
        if val {
            enemy_log!("Large fish is retreating");
        }
        val
    }
    fn print_opponent(&self) -> String {
        String::from(
            "@@@@@%*##**#+*##**#%%@@*+                              @                                                               
@@@%%@#*++****+++++++*###*%%%@@@#              @@*=*%%@#@#                                                             
@+#****+++++++++++++++*+++=+*++*@@@@@@@@@#@%=*@%%%%%%#                                                                 
*####****++++++++++=========-=+=++*#%@%=+%%@%%%%%%%##                                                                  
########******+++++++++===++=====+==*%%%%%%%%%%%%###@@@                                                                
#%%########*******+++*+**********####%%%%%%%%%%%###*@@@@@@@@@@@               @@@@@@                                   
#%%%#%#########**#****************#################*=*+@@@@@@@@@@@@@@@    @@@@@@@@@@@                                  
 %%%%%%%%###############**********##################===+@%@@@@@@@@@@@@@@@@@@@@@@@@@@@                                  
 %%%@%%%%%%%%%%%%%%#########*###****########**+#****+++++++@%%@@@@@@@@@@@@@@@@@@@@@@@@                                 
   %@%%%%%%%%%%%%%%%%%%%%################*****++++++++*****++++*##@@@@@@@@@@@@@@@@@@@@@@                               
       #%%%%%%%%%%%%%%%%%%%%%%%%%%##########*#*****************+*+++#+##@@@@@@@@@@@@@@@@@@@@@                          
           %%%%%%%%%%%%%%%%%%%%@@%%%%%%%%#%###############**##******+++++++#*%@@@@@@@@@@@@@@@@@@@@       @@@           
                %%@@%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%########***********++++++*@@@@@@@@@%##%%@@@@@@@@@@@@          
                        %%%%%%%%%%%@@@@%%%%%%%%%%%%%@@@@@@@%%%%%%######*************####%@@@@@@%###########***+*#####**
                              +**%@@@@@@@@@@@@@@@@%%%%@@%%%%%@@@@@@@@%%%##**+++++*#%@@%###############%################
                                    *@@%%%%@@@@@@@%%@@@@@@@@@@@@@%%%%@@@@@@@@@@%%@@@@*#%%%%%%%%%%%%%%%%%%%%%#####%%%%%%
                                       %%%%%%%%%%@           @@@@@@@@@@@@%%@%%%#%@%%%%%%#**%@@@@@@@@@@@@%%##**+*#@@@@@@
                                          %%#%%%%%%%                     @@%#%%%%%%%%@@%%#%@@@@@@@@@@@@@%###*@@@@@@    
                                            *%#######*                      @########%    @                            
                                                %#****+*                        **#                                    
                                                     ++=@                                                              "
        )
    }
    fn return_limbs(&self) -> Vec<u8> {
        let mut out=vec![0,1];
        if self.tail>0 {out.push(2)}
        out
    }
}

/*#[derive(Debug, Clone)]
pub struct FishPack {
    small_fish: Vec<SmallFish>,
    medium_fish: Vec<MediumFish>,
    large_fish: Vec<LargeFish>,
    original_size: u8,
    retreated: u8,
}
impl FishPack {
    pub fn new(difficulty: u8) -> Self {
        let mut small_fish=vec![];
        let mut medium_fish=vec![];
        let mut large_fish=vec![];
        if difficulty<20 {
            for _ in 0..random_value(1, difficulty/3+1) {
                small_fish.push(SmallFish::new())
            }
        }
        else if difficulty<50 {
            for _ in 0..random_value(1, difficulty/20+1) {
                small_fish.push(SmallFish::new())
            }
            for _ in 0..random_value(1, difficulty/10+1) {
                medium_fish.push(MediumFish::new())
            }
        }
        else if difficulty<100 {
            for _ in 0..random_value(1, difficulty/30+1) {
                medium_fish.push(MediumFish::new())
            }
            for _ in 0..random_value(1, difficulty/50+1) {
                large_fish.push(LargeFish::new())
            }
        }
        else {
            for _ in 0..random_value(1, difficulty/50) {
                large_fish.push(LargeFish::new())
            }
        }
        let size=(small_fish.len()+medium_fish.len()+large_fish.len()) as u8;
        Self {
            small_fish: (small_fish),
            medium_fish: (medium_fish),
            large_fish: (large_fish),
            original_size: (size),
            retreated: (0)
        }
    }
}
impl EnemyBehaviour for FishPack {
    fn deal_damage(&self) -> Vec<u8> {
        let mut out=vec![];
        for fish in &self.small_fish {
            out.append(&mut fish.deal_damage());
        }
        for fish in &self.medium_fish {
            out.append(&mut fish.deal_damage());
        }
        for fish in &self.large_fish {
            out.append(&mut fish.deal_damage());
        }
        out
    }
    fn receive_damage(&mut self, amount: u8, part: u8) -> bool {
        // 0,1,2 for inner fish targetting comes from first character
        // which fish to target comes from the second two characters
        let fish_ind=part%100;
        let fish_limb=part/100;
        if fish_ind<self.small_fish.len() as u8 {
            if self.small_fish[fish_ind as usize].receive_damage(amount, fish_limb) {
                self.small_fish.remove(fish_ind as usize);
            }
        }
        else if fish_ind<self.small_fish.len() as u8 + self.medium_fish.len() as u8 {
            let ind=fish_ind as usize-self.small_fish.len();
            if self.medium_fish[ind].receive_damage(amount, fish_limb) {
                self.medium_fish.remove(ind);
            }
        }
        else {
            let ind=fish_ind as usize-self.small_fish.len()-self.medium_fish.len();
            if self.large_fish[ind].receive_damage(amount, fish_limb) {
                self.large_fish.remove(ind);
            }
        }
        self.small_fish.len()+self.medium_fish.len()+self.large_fish.len()==0
    }
    fn retreat(&mut self) -> bool {
        let mut s=vec![];
        swap(&mut s, &mut self.small_fish);
        for mut fish in s {
            if !fish.retreat() {
                self.small_fish.push(fish);
            }
            else {
                self.retreated+=1;
            }
        }
        let mut s=vec![];
        swap(&mut s, &mut self.medium_fish);
        for mut fish in s {
            if !fish.retreat() {
                self.medium_fish.push(fish);
            }
            else {
                self.retreated+=1;
            }
        }
        let mut s=vec![];
        swap(&mut s, &mut self.large_fish);
        for mut fish in s {
            if !fish.retreat() {
                self.large_fish.push(fish);
            }
            else {
                self.retreated+=1;
            }
        }
        let val=self.retreated>self.original_size/2 || self.small_fish.len()+self.medium_fish.len()+self.large_fish.len()<self.original_size as usize/2;
        if val {
            enemy_log!("Pack is retreating")
        }
        val
    }
    fn print_opponent(&self) -> String {
        todo!()
    }
}
*/
#[derive(Debug, Clone)]
pub struct Guard {
    head: (u8,u8,u8),
    torso: u8,
    tail: (u8,u8),
    head_damage: u8,
    damage: u8,
    tail_damage: u8,
    regeneration: u8
}
impl Guard {
    pub fn new() -> Self {
        Self {
            head: ((random_value(7, 13)),(random_value(10, 16)),(random_value(7, 13))),
            torso: (random_value(52, 78)),
            tail: ((random_value(10, 17),random_value(8, 13))),
            head_damage: (random_value(5, 9)),
            damage: (random_value(20, 42)),
            tail_damage: (random_value(8, 22)),
            regeneration: (random_value(0, 3))
        }
    }
}
impl EnemyBehaviour for Guard {
    fn deal_damage(&self) -> Vec<u8> {
        let r=random_value(0, 100);
        // Normal damage
        if r<40 {
            enemy_log!("Guard attacks with its body");
            vec![self.damage]
        }
        // Tail damage
        else if r<80 {
            if r<60 {
                if self.tail.0>0 {
                    enemy_log!("Guard attacks with its left tail");
                    vec![self.tail_damage]
                }
                else {
                    enemy_log!("Guard waits for an opening");
                    vec![]
                }
            }
            else {
                if self.tail.1>0 {
                    enemy_log!("Guard attacks with its right tail");
                    vec![self.tail_damage]
                }
                else {
                    enemy_log!("Guard observes Carp");
                    vec![]
                }
            }
        }
        // Head damage
        else {
            if r<85 {
                if self.head.0>0 {
                    enemy_log!("Guard attacks with its left head");
                    vec![self.head_damage]
                }
                else {
                    enemy_log!("Guard tries to stop Carp");
                    vec![]
                }
            }
            else if r<95 {
                if self.head.1>0 {
                    enemy_log!("Guard attacks with its middle head");
                    vec![self.head_damage]
                }
                else {
                    enemy_log!("Guard is in pain");
                    vec![]
                }
            }
            else {
                if self.head.2>0 {
                    enemy_log!("Guard attacks with its right head");
                    vec![self.head_damage]
                }
                else {
                    enemy_log!("Guard is ready to fight");
                    vec![]
                }
            }
        }
    }
    fn receive_damage(&mut self, mut amount: u8, part: u8) -> bool {
        amount=if amount>=self.regeneration {amount-self.regeneration} else {0};
        if amount==0 { return false; }
        match part {
            0 => {
                let r=random_value(0, 100);
                if r<92 {
                    self.torso=damage_component(self.torso, amount);
                    let val=
                    if self.torso>60 {
                        5
                    } else if self.torso==0 {
                        0
                    } else if self.torso<10 {
                        1
                    } else { 4 };
                    enemy_log!("Guard's torso was damaged",val);
                    self.torso==0
                }
                else {
                    enemy_log!("Guard managed to avoid the attack");
                    false
                }
            },
            1 => {
                let mut h=random_value(0, 3);
                if h==0 && self.head.0==0 {
                    h+=1;
                }
                if h==1 && self.head.1==0 {
                    h+=1;
                }
                if h==2 && self.head.2==0 {
                    h=if self.head.0==0 {1} else {0};
                }
                let r=random_value(0, 100);
                match h {
                    0 => {
                        if r<65 {
                            self.head.0=damage_component(self.head.0, amount);
                            let val=
                            if self.head.0>7 {
                                5
                            } else if self.head.0==0 {
                                0
                            } else if self.head.0<3 {
                                1
                            } else { 4 };
                            enemy_log!("Guard's right head was damaged",val);
                            self.head.0==0 && self.head.1==0 && self.head.2==0
                        }
                        else {
                            enemy_log!("Guard managed to move its head out of harm's way");
                            false
                        }
                    }
                    1 => {
                        if r<50 {
                            self.head.1=damage_component(self.head.1, amount);
                            let val=
                            if self.head.1>10 {
                                5
                            } else if self.head.1==0 {
                                0
                            } else if self.head.1<5 {
                                1
                            } else { 4 };
                            enemy_log!("Guard's middle head was damaged",val);
                            self.head.0==0 && self.head.1==0 && self.head.2==0
                        }
                        else {
                            enemy_log!("Guard moved its head in a supernatural way");
                            false
                        }
                    }
                    _ => {
                        if r<65 {
                            self.head.2=damage_component(self.head.2, amount);
                            let val=
                            if self.head.2>7 {
                                5
                            } else if self.head.2==0 {
                                0
                            } else if self.head.2<3 {
                                1
                            } else { 4 };
                            enemy_log!("Guard's left head was damaged",val);
                            self.head.0==0 && self.head.1==0 && self.head.2==0
                        }
                        else {
                            enemy_log!("Guard's head dodged the attack");
                            false
                        }
                    }
                }
                
            }
            _ => {
                let mut h=random_value(0, 2);
                if h==0 && self.tail.0==0 {
                    h=1;
                }
                if h==1 && self.tail.1==0 {
                    h=0;
                }
                let r=random_value(0, 100);
                match h {
                    0 => {
                        if r<80 {
                            self.tail.0=damage_component(self.tail.0, amount);
                            let val=
                            if self.tail.0>8 {
                                5
                            } else if self.tail.0==0 {
                                0
                            } else if self.tail.0<4 {
                                1
                            } else { 4 };
                            enemy_log!("Guard's left tail was damaged",val);
                        }
                        else {
                            enemy_log!("Guard's tail quietly dodged the attack");
                        }
                    }
                    _ => {
                        if r<65 {
                            self.tail.1=damage_component(self.tail.1, amount);
                            let val=
                            if self.tail.0>10 {
                                5
                            } else if self.tail.0==0 {
                                0
                            } else if self.tail.0<5 {
                                1
                            } else { 4 };
                            enemy_log!("Guard's right tail was damaged",val);
                        }
                        else {
                            enemy_log!("Guard's tail rapidly moved out of the way");
                        }
                    }
                }
                
                false
            }
        }
    }
    fn retreat(&mut self) -> bool {
        false
    }
    fn print_opponent(&self) -> String {
        String::from(
"    -::::........+#@%@#=+@%=++*%#%@*##**@==+-+@:.:+*-:##-%%--::@#--=@*--%=-+-..:+**#=+@@@%*+                           
    :.:..:.....:*#*%###*##**++*##=+%@@%**%----...----.::%%#=:::::::::+*-%@=*--++.+@@.:%=+###*                          
   ::......:..#@@%@%#*+*##%**#*---=+*=%::-=+*:@@-.::#-:::::=+---:::::-=---==*+==*++**...::+.:=                         
   ::........@#%*##@**#%#@+**@=.....:.....+-++=-.#*=::::::-#*::::+%::--@%-====-.=-+*+.....+:.=                         
   ::.......@*#******%@##%***+=...........:--:::----::::-:-:....-=--=%#-.----====-+==@....+-%@                         
   ::......%#+**#**+@@##@#@+=-::..........-::+#:--%::=..:..----------+=---::-.%===++%=+...:-*@@                        
   ::......*+*@%@**@@%@#%##==-:.--:.......:.:=:=+::::-.-::::...--=:.:::....::-=+:-=-===*...%%%%%*                      
   ::.....++%%***=@%@###+==--=-.....................+=-:...................:::-==+*+=@-=+..*@%%@@%                     
   :......*%++=-*%%%#*+=*%=---.............:..-..*+-::.........................:--=@#--@%-.@@##%%@@                    
   -:....*++*++%%#@+++*#*----..................%*-:...........................:...--#+*:=*=####@%%@%                   
   ::...#=+*#=@@**#=#*%=---:.................*==-.....:.............................-#*++**##@##@#%@                   
   :...+-*+#=%%+++++#=--:=:........:....::.#-.::....................................::+-=*%+#@##%%%%@                  
   :..-:=+-+#==+-==*#--:=-............:..:=.....:-:........................::::......:..--*=#*@@*@#%%@                 
  -:..:---+%*+=--=-=-:..............:-..*:.....::..........--......................:...:.--+%@:%#**#%@@                
  --.-::=*-+::.--::................::..=............-.................................--..:-*@@=-@@@%%@@               
   -.::##-.:...-............:.........:...:......:....:..........:.....................:-=.:%@@@:-=*#%%@         +%@@%%
   -::=+*.::..:.....................:.........:-........................................:+%.=%@@%.:+**#@@   *@**+*+===#
   =::-::.::::.........:.....................=::....................:..:......::.........:-=-=#@@@.:=+#%@ %*#-#=-::*#-.
   ---:-..:::.:.::..................:......:..::....:::.::::::.:::.:-:....::.:-:..........:-=*=@@@@.:-@#@@=.::...:...++
    :::..:--:::::..................:.:........:::::---:.:--:::-=-=-++=:-:::==-:::::..:.:::..:=*@@@%..:=#@-.:...........
@   :::..:---:---.............:.....:::...:::::-===:::::--=-::---==--=-----------::::::...::::=*+%@%..-+@#........:-+:.
+#@*-::.:.----=-=................::-:-.::::--===::::-::-----===+=*==-----====-==---==-::::.::::--*#@:.=+%+.....:....-+%
-*=+-:-.-:-======::......:..-:..::---:--===+-..:-=-=--===-==+++=====+++===-+===---------::::.::=:+*@+:=-+=......:-:-...
*=-=-::.:.:=++=++-:.....:....::::-==--=++=.:-==--===========+=++=*+===--==:+++++=----------::.:::*+#+-:-*+........:-=*%
+=-=-::.=:=*#**===-:::..:==:.::::-:--=++.:--===+++++*==+===*#*=++*#+======-++++==--=--=-=----.-::-++-::-+...-*+-.......
++++==:.:+*=*###**=-:::.-===:::::-::===:++=++++++**+=++=**+=+++=***+++++==+*++**+==-=++=-----.:.::------:......-*+*%++=
-==+===-.----+*%#**=====--=-::::--:.-+.++***++++####+=+++++++*++*+++++++=-+*++===+==------=----:::----::........:....:.
:-==+=+--:=-====#%#*=+#*==--::::::::=:+++++++*++++====++**+++*=+**++++===***++++++========---:-:.----:::....:::.:::::..
:---=+===-:.==*==++*+=*#**==---:-:-=:=+*****+***+++*+**##%*#*++=*#***+++-#**+++++++++=======--=:----.:-............:---
--:=--+-=+*=:.++++=++**++*#++=-=-==*=++***+***+**+*#***###***+=*####+++=-##*%%*#+*++++++=====----=..:...............:..
:--------+++=-:..=+#*+*##**#*+++==++++********#***+*********=#*******++=+%%*#****+**+*++========...-..................."
        )
    }
    fn return_limbs(&self) -> Vec<u8> {
        let mut out=vec![0,1];
        if self.tail.0>0 || self.tail.1>0 {out.push(2)}
        out
    }
}

/*#[derive(Debug, Clone)]
pub struct Boss {
    head: u8,
    torso: u8,
    tail: (u8,u8,u8),
    head_damage: u8,
    damage: u8,
    tail_damage: u8,
    regeneration: u8
}

#[derive(Debug, Clone)]
pub struct GuardPack {
    guards: Vec<Guard>
}

#[derive(Debug, Clone)]
pub struct DepthGuards {
    guards: GuardPack,
    boss: Boss
}*/